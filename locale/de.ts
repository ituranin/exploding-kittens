<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en_GB">
<context>
    <name>CardsToBePlayed</name>
    <message>
        <location filename="../CardsToBePlayed.qml" line="294"/>
        <source>Play Selected</source>
        <translation>Gewählte spielen</translation>
    </message>
</context>
<context>
    <name>ChooseKittenPosition</name>
    <message>
        <location filename="../ChooseKittenPosition.qml" line="25"/>
        <source>Choose where to place the Kitten:</source>
        <translation>Position der Katzenkarte wählen:</translation>
    </message>
    <message>
        <location filename="../ChooseKittenPosition.qml" line="97"/>
        <source>Set position</source>
        <translation>Position bestätigen</translation>
    </message>
</context>
<context>
    <name>Client</name>
    <message>
        <location filename="../src/client.cpp" line="145"/>
        <source>You were kicked!</source>
        <translation>Du wurdest rausgeworfen!</translation>
    </message>
    <message>
        <location filename="../src/client.cpp" line="245"/>
        <source>Lobby is full!</source>
        <translation>Die Lobby ist voll!</translation>
    </message>
    <message>
        <location filename="../src/client.cpp" line="252"/>
        <source>You can&apos;t join a running game!</source>
        <translation>Einem laufenden Spiel kann nicht beigetreten werden!</translation>
    </message>
</context>
<context>
    <name>CreateProfile</name>
    <message>
        <location filename="../CreateProfile.qml" line="24"/>
        <source>Choose a Nickname:</source>
        <translation>Wähle einen Spielernamen:</translation>
    </message>
    <message>
        <location filename="../CreateProfile.qml" line="64"/>
        <source>PLAYER</source>
        <translation>SPIELER</translation>
    </message>
    <message>
        <location filename="../CreateProfile.qml" line="88"/>
        <source>Create</source>
        <translation>Erstellen</translation>
    </message>
</context>
<context>
    <name>EndGameState</name>
    <message>
        <source>nobody</source>
        <extracomment>This name refers to the standart unset mostDefusesPlayer in the statistics pre examination
----------
This name refers to the standart unset mostNopePlayer in the statistics pre examination
----------
This name refers to the standart unset mostCardsPlayer in the statistics pre examination
----------
This name refers to the standart unset mostSpecialMovesPlayer in the statistics pre examination</extracomment>
        <translation type="vanished">niemand</translation>
    </message>
</context>
<context>
    <name>EndGameStats</name>
    <message>
        <location filename="../EndGameStats.qml" line="48"/>
        <source>Game Statistics</source>
        <translation>Spielstatistiken</translation>
    </message>
    <message>
        <location filename="../EndGameStats.qml" line="82"/>
        <source>Addicted</source>
        <translation>Süchtig</translation>
    </message>
    <message>
        <location filename="../EndGameStats.qml" line="99"/>
        <location filename="../EndGameStats.qml" line="157"/>
        <location filename="../EndGameStats.qml" line="216"/>
        <location filename="../EndGameStats.qml" line="275"/>
        <source>PLAYER</source>
        <translation>SPIELER</translation>
    </message>
    <message>
        <location filename="../EndGameStats.qml" line="140"/>
        <source>Bomb Squad</source>
        <translation>Entschärfungs-Kommando</translation>
    </message>
    <message>
        <location filename="../EndGameStats.qml" line="199"/>
        <source>Nay Sayer</source>
        <translation>Nein-Sager</translation>
    </message>
    <message>
        <location filename="../EndGameStats.qml" line="258"/>
        <source>&apos;Special&apos;</source>
        <translation>&apos;Speziell&apos;</translation>
    </message>
</context>
<context>
    <name>ErrorMessage</name>
    <message>
        <location filename="../ErrorMessage.qml" line="29"/>
        <source>Text</source>
        <translation>Text</translation>
    </message>
</context>
<context>
    <name>ExplodingKitten</name>
    <message>
        <location filename="../ExplodingKitten.qml" line="36"/>
        <source>Exploding Kitten drawn!</source>
        <translation>Explosive Katze gezogen!</translation>
    </message>
    <message>
        <location filename="../ExplodingKitten.qml" line="124"/>
        <source>Defuse!</source>
        <translation>Entschärfen!</translation>
    </message>
</context>
<context>
    <name>GameForm</name>
    <message>
        <location filename="../GameForm.qml" line="431"/>
        <source>Quit Session</source>
        <translation>Sitzung verlassen</translation>
    </message>
</context>
<context>
    <name>HostGameForm</name>
    <message>
        <location filename="../HostGameForm.qml" line="129"/>
        <source>waiting for players</source>
        <translation>Warte auf Spieler</translation>
    </message>
    <message>
        <location filename="../HostGameForm.qml" line="64"/>
        <source>Start Local Host</source>
        <translation>Server starten</translation>
    </message>
    <message>
        <location filename="../HostGameForm.qml" line="95"/>
        <source>Start Game</source>
        <translation>Spiel starten</translation>
    </message>
</context>
<context>
    <name>JoinGameForm</name>
    <message>
        <location filename="../JoinGameForm.qml" line="76"/>
        <source>IP:</source>
        <translation>IP:</translation>
    </message>
    <message>
        <location filename="../JoinGameForm.qml" line="102"/>
        <source>Join Game</source>
        <translation>Beitreten</translation>
    </message>
    <message>
        <location filename="../JoinGameForm.qml" line="173"/>
        <source>Leave</source>
        <translation>Verlassen</translation>
    </message>
</context>
<context>
    <name>LobbyPlayer</name>
    <message>
        <location filename="../LobbyPlayer.qml" line="18"/>
        <source>PLAYER</source>
        <translation>SPIELER</translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="../MainMenu.qml" line="272"/>
        <source>until</source>
        <translation>bis</translation>
    </message>
    <message>
        <location filename="../MainMenu.qml" line="276"/>
        <source>level </source>
        <translation>Level </translation>
    </message>
    <message>
        <location filename="../MainMenu.qml" line="340"/>
        <source> played:</source>
        <translation> spielte:</translation>
    </message>
    <message>
        <location filename="../MainMenu.qml" line="363"/>
        <source>from top to bottom</source>
        <translation>Von Oben nach Unten</translation>
    </message>
    <message>
        <location filename="../MainMenu.qml" line="494"/>
        <source>Join Game</source>
        <translation>Beitreten</translation>
    </message>
    <message>
        <location filename="../MainMenu.qml" line="521"/>
        <source>Host Game</source>
        <translation>Erstellen</translation>
    </message>
    <message>
        <location filename="../MainMenu.qml" line="548"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location filename="../MainMenu.qml" line="575"/>
        <source>Statistics</source>
        <translation>Statistiken</translation>
    </message>
    <message>
        <location filename="../MainMenu.qml" line="602"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
</context>
<context>
    <name>OptionsMenuForm</name>
    <message>
        <location filename="../OptionsMenuForm.qml" line="48"/>
        <location filename="../OptionsMenuForm.qml" line="76"/>
        <location filename="../OptionsMenuForm.qml" line="287"/>
        <location filename="../OptionsMenuForm.qml" line="305"/>
        <location filename="../OptionsMenuForm.qml" line="517"/>
        <source>English</source>
        <translation>Englisch</translation>
    </message>
    <message>
        <location filename="../OptionsMenuForm.qml" line="50"/>
        <location filename="../OptionsMenuForm.qml" line="78"/>
        <location filename="../OptionsMenuForm.qml" line="302"/>
        <source>German</source>
        <translation>Deutsch</translation>
    </message>
    <message>
        <location filename="../OptionsMenuForm.qml" line="54"/>
        <location filename="../OptionsMenuForm.qml" line="82"/>
        <location filename="../OptionsMenuForm.qml" line="536"/>
        <source>Off</source>
        <translation>Aus</translation>
    </message>
    <message>
        <location filename="../OptionsMenuForm.qml" line="57"/>
        <location filename="../OptionsMenuForm.qml" line="85"/>
        <location filename="../OptionsMenuForm.qml" line="532"/>
        <source>On</source>
        <translation>An</translation>
    </message>
    <message>
        <location filename="../OptionsMenuForm.qml" line="157"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../OptionsMenuForm.qml" line="173"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../OptionsMenuForm.qml" line="189"/>
        <source>FX volume</source>
        <translation>Lautstärke FX</translation>
    </message>
    <message>
        <location filename="../OptionsMenuForm.qml" line="205"/>
        <source>Music volume</source>
        <translation>Lautstärke Musik</translation>
    </message>
    <message>
        <location filename="../OptionsMenuForm.qml" line="221"/>
        <source>Fullscreen</source>
        <translation>Vollbild</translation>
    </message>
    <message>
        <location filename="../OptionsMenuForm.qml" line="574"/>
        <source>Apply</source>
        <translation>Übernehmen</translation>
    </message>
    <message>
        <location filename="../OptionsMenuForm.qml" line="606"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>Player</name>
    <message>
        <location filename="../Player.qml" line="24"/>
        <source>PLAYER</source>
        <translation>SPIELER</translation>
    </message>
</context>
<context>
    <name>PlayerPlayedCards</name>
    <message>
        <location filename="../PlayerPlayedCards.qml" line="90"/>
        <source>Text</source>
        <translation>Text</translation>
    </message>
</context>
<context>
    <name>StatisticsMenuForm</name>
    <message>
        <location filename="../StatisticsMenuForm.qml" line="28"/>
        <source>cards played</source>
        <translation>Karten gespielt</translation>
    </message>
    <message>
        <location filename="../StatisticsMenuForm.qml" line="35"/>
        <source>wins</source>
        <translation>Siege</translation>
    </message>
    <message>
        <location filename="../StatisticsMenuForm.qml" line="42"/>
        <source>losses</source>
        <translation>Niederlagen</translation>
    </message>
    <message>
        <location filename="../StatisticsMenuForm.qml" line="49"/>
        <source>times defused</source>
        <translation>Entschärfungen</translation>
    </message>
    <message>
        <location filename="../StatisticsMenuForm.qml" line="56"/>
        <source>times noped</source>
        <translation>Nopes</translation>
    </message>
    <message>
        <location filename="../StatisticsMenuForm.qml" line="63"/>
        <source>special actions</source>
        <translation>Spezielle Aktionen</translation>
    </message>
    <message>
        <location filename="../StatisticsMenuForm.qml" line="70"/>
        <source>last played againts:</source>
        <translation>letzte Gegner:</translation>
    </message>
</context>
</TS>
