import QtQuick 2.0
import QtQuick.Particles 2.0

Item {
    ParticleSystem {
        id: particle_system
    }

    Emitter {
        id: particle_emitter
        system: particle_system
        //anchors.centerIn: parent
        width: parent.width
        x: 0
        y: 0
        emitRate: 10
        lifeSpan: 10000
        //lifeSpanVariation: 8000
        size: 64
        //endSize: 128
        velocity: AngleDirection {
                    angle: 80
                    angleVariation: -20
                    magnitude: 100
                    //magnitudeVariation: 100
                }
    }

    ImageParticle {
        source: "qrc:///leafParticle"
        system: particle_system
        smooth: false
        rotation: 0
        rotationVariation: 360
    }

}
