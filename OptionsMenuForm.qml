import QtQuick 2.0
import SoundTypes 1.0
import QtQuick.Window 2.2

Item {
    id: item1
    property var lang:;
    property var volFX:;
    property var volMUSIC:;
    property var full:;
    property var name:;

    property var newLang:;
    property var newVolFX:;
    property var newVolMUSIC:;
    property var newFull:;
    property var newName:;

    /*
        \qmlmethod updateName(updated)

        This method updates the text in the text input for player name change
    */
    function updateName(updated) {
        name = newName =  updated;
        nameTextInput.text = newName;
    }

    /*
        \qmlmethod loadSettings(settings)

        This method sets all labels and buttons and inputs based on the settings given through the parameter
    */
    function loadSettings(settings) {
        lang = settings[0];
        volFX = settings[1];
        volMUSIC = settings[2];
        full = settings[3];
        name = settings[4]

        newLang = settings[0];
        newVolFX = settings[1];
        newVolMUSIC = settings[2];
        newFull = settings[3];
        newName = settings[4];

        if(newLang == 0) {
            langbuttontext.text = qsTr("English") + gameAdapter.translation;
        } else {
            langbuttontext.text = qsTr("German") + gameAdapter.translation;
        }

        if(newFull == 0) {
            fsbuttontext.text = qsTr("Off") + gameAdapter.translation;
            fsbutton.color = "#99d14141";
        } else {
            fsbuttontext.text = qsTr("On") + gameAdapter.translation;
            fsbutton.color = "#99bada55";
        }

        volFXText.text = newVolFX;

        volMUSICText.text = newVolMUSIC;

        nameTextInput.text = newName;
    }

    /*
        \qmlmethod reloadSettings()

        This method re-sets the labels and inputs
    */
    function reloadSettings() {

        if(newLang == 0) {
            langbuttontext.text = qsTr("English") + gameAdapter.translation;
        } else {
            langbuttontext.text = qsTr("German") + gameAdapter.translation;
        }

        if(newFull == 0) {
            fsbuttontext.text = qsTr("Off") + gameAdapter.translation;
            fsbutton.color = "#99d14141";
        } else {
            fsbuttontext.text = qsTr("On") + gameAdapter.translation;
            fsbutton.color = "#99bada55";
        }

        volFXText.text = newVolFX;

        volMUSICText.text = newVolMUSIC;

        nameTextInput.text = newName;
    }

    /*
        \qmlmethod applySettings()

        This method applies the chosen settings in gui and the rest of the game
    */
    function applySettings() {
        lang = newLang;
        volFX = newVolFX;
        volMUSIC = newVolMUSIC;
        full = newFull;
        name = newName;

        if(full == 1) {
            root.visibility = Window.FullScreen;
        } else {
            root.visibility = Window.Windowed;
        }
        gameAdapter.saveSettings(lang, volFX, volMUSIC, full, name);
        main_form.updateStatsTab();
    }

    /*
        \qmlmethod discardSettings()

        This method discards all changes made in the options
    */
    function discardSettings() {
        newLang = lang;
        newVolFX = volFX;
        newVolMUSIC = volMUSIC;
        newFull = full;
        newName = name;
        reloadSettings();
    }

    Rectangle {
        id: settings
        color: "#00000000"
        anchors.bottom: buttons.top
        anchors.bottomMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0

        Rectangle {
            id: labels
            width: parent.width * 0.5
            color: "#00000000"
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0

            Text {
                id: nameLabel
                height: parent.height * 0.20
                text: qsTr("Name") + gameAdapter.translation
                font.bold: true
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.top: parent.top
                anchors.topMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                font.pixelSize: 22
            }

            Text {
                id: language
                height: parent.height * 0.20
                text: qsTr("Language") + gameAdapter.translation
                font.bold: true
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                anchors.top: nameLabel.bottom
                anchors.topMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                font.pixelSize: 22
            }

            Text {
                id: volumeFX
                height: parent.height * 0.20
                text: qsTr("FX volume") + gameAdapter.translation
                font.bold: true
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.top: language.bottom
                anchors.topMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                font.pixelSize: 22
            }

            Text {
                id: volumeMusic
                height: parent.height * 0.20
                text: qsTr("Music volume") + gameAdapter.translation
                font.bold: true
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.top: volumeFX.bottom
                anchors.topMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                font.pixelSize: 22
            }

            Text {
                id: fs
                height: parent.height * 0.20
                text: qsTr("Fullscreen") + gameAdapter.translation
                font.bold: true
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.top: volumeMusic.bottom
                anchors.topMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                font.pixelSize: 22
            }
        }

        Rectangle {
            id: inputs
            width: parent.width * 0.5
            color: "#00000000"
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0

            Rectangle {
                id: nameInput
                height: parent.height * 0.20
                color: "#99000000"
                anchors.top: parent.top
                anchors.topMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0

                TextInput {
                    id: nameTextInput
                    color: "#ededed"
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    anchors.verticalCenter: parent.verticalCenter
                    cursorVisible: true
                    echoMode: TextInput.Normal
                    font.bold: true
                    font.pointSize: 22
                    horizontalAlignment: Text.AlignHCenter
                    onTextChanged: newName = nameTextInput.text
                }
            }

            Rectangle {
                id: langbutton
                height: parent.height * 0.20
                color: "#99bada55"
                anchors.top: nameInput.bottom
                anchors.topMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                Text {
                    id: langbuttontext
                    color: "#ededed"
                    text: qsTr("English") + gameAdapter.translation
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: 22
                    verticalAlignment: Text.AlignVCenter
                    font.bold: true
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                }

                MouseArea {
                    id: mouseArea3
                    hoverEnabled: true
                    anchors.fill: parent
                    onClicked: {
                        if(newLang == 0) {
                            langbuttontext.text = qsTr("German") + gameAdapter.translation;
                            newLang = 1;
                        } else {
                            langbuttontext.text = qsTr("English") + gameAdapter.translation;
                            newLang = 0;
                        }
                    }
                }
            }

            Rectangle {
                id: volFXInput
                height: parent.height * 0.20
                color: "#99000000"
                anchors.top: langbutton.bottom
                anchors.topMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0

                Text {
                    id: volFXText
                    x: 0
                    text: "0"
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 0
                    anchors.top: parent.top
                    anchors.topMargin: 0
                    verticalAlignment: Text.AlignVCenter
                    font.bold: true
                    color: "#ededed"
                    horizontalAlignment: Text.AlignHCenter
                    anchors.right: volFXPlus.left
                    anchors.rightMargin: 0
                    anchors.left: volFXMinus.right
                    anchors.leftMargin: 0
                    font.pixelSize: 22
                }

                Rectangle {
                    id: volFXMinus
                    width: parent.width * 0.2
                    color: "#99d14141"
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 0
                    Text {
                        id: volFXMinusText
                        color: "#ededed"
                        text: "-"
                        anchors.fill: parent
                        font.pixelSize: 22
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                        horizontalAlignment: Text.AlignHCenter
                    }

                    MouseArea {
                        id: mouseArea5
                        hoverEnabled: true
                        anchors.fill: parent
                        onClicked: {
                            if(newVolFX > 0) {
                                newVolFX--;
                                volFXText.text = newVolFX;
                            }
                        }
                    }
                    anchors.topMargin: 0
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.leftMargin: 0
                }

                Rectangle {
                    id: volFXPlus
                    width: parent.width * 0.2
                    color: "#99bada55"
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    anchors.bottom: parent.bottom
                    Text {
                        id: volFXPlusText
                        color: "#ededed"
                        text: "+"
                        font.pixelSize: 22
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                    }

                    MouseArea {
                        id: mouseArea6
                        hoverEnabled: true
                        anchors.fill: parent
                        onClicked: {
                            if(newVolFX < 100) {
                                newVolFX++;
                                volFXText.text = newVolFX;
                            }
                        }
                    }
                    anchors.topMargin: 0
                    anchors.top: parent.top
                    anchors.bottomMargin: 0
                }
            }

            Rectangle {
                id: volMUSICInput
                height: parent.height * 0.20
                color: "#99000000"
                anchors.right: parent.right
                anchors.rightMargin: 0
                Text {
                    id: volMUSICText
                    x: 0
                    color: "#ededed"
                    text: "0"
                    anchors.right: volMUSICPlus.left
                    anchors.bottom: parent.bottom
                    font.pixelSize: 22
                    anchors.rightMargin: 0
                    verticalAlignment: Text.AlignVCenter
                    anchors.topMargin: 0
                    anchors.left: volMUSICMinus.right
                    anchors.top: parent.top
                    anchors.leftMargin: 0
                    anchors.bottomMargin: 0
                    font.bold: true
                    horizontalAlignment: Text.AlignHCenter
                }

                Rectangle {
                    id: volMUSICMinus
                    width: parent.width * 0.2
                    color: "#99d14141"
                    anchors.bottom: parent.bottom
                    Text {
                        id: volMUSICMinusText
                        color: "#ededed"
                        text: "-"
                        font.pixelSize: 22
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                    }

                    MouseArea {
                        id: mouseArea7
                        hoverEnabled: true
                        anchors.fill: parent
                        onClicked: {
                            if(newVolMUSIC > 0) {
                                newVolMUSIC--;
                                volMUSICText.text = newVolMUSIC;
                            }
                        }
                    }
                    anchors.topMargin: 0
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.leftMargin: 0
                    anchors.bottomMargin: 0
                }

                Rectangle {
                    id: volMUSICPlus
                    width: parent.width * 0.2
                    color: "#99bada55"
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    Text {
                        id: volMUSICPlusText
                        color: "#ededed"
                        text: "+"
                        font.pixelSize: 22
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                    }

                    MouseArea {
                        id: mouseArea8
                        hoverEnabled: true
                        anchors.fill: parent
                        onClicked: {
                            if(newVolMUSIC < 100) {
                                newVolMUSIC++;
                                volMUSICText.text = newVolMUSIC;
                            }
                        }
                    }
                    anchors.topMargin: 0
                    anchors.top: parent.top
                    anchors.bottomMargin: 0
                }
                anchors.topMargin: 0
                anchors.left: parent.left
                anchors.top: volFXInput.bottom
                anchors.leftMargin: 0
            }

            Rectangle {
                id: fsbutton
                height: parent.height * 0.20
                anchors.right: parent.right
                anchors.rightMargin: 0
                Text {
                    id: fsbuttontext
                    color: "#ededed"
                    text: qsTr("English") + gameAdapter.translation
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: 22
                    verticalAlignment: Text.AlignVCenter
                    font.bold: true
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                }

                MouseArea {
                    id: mouseArea4
                    hoverEnabled: true
                    anchors.fill: parent
                    onClicked: {
                        if(newFull == 0) {
                            fsbuttontext.text = qsTr("On") + gameAdapter.translation;
                            fsbutton.color = "#99bada55";
                            newFull = 1;
                        } else {
                            fsbuttontext.text = qsTr("Off") + gameAdapter.translation;
                            fsbutton.color = "#99d14141";
                            newFull = 0;
                        }
                    }
                }
                anchors.topMargin: 0
                anchors.left: parent.left
                anchors.top: volMUSICInput.bottom
                anchors.leftMargin: 0
            }

        }
    }

    Rectangle {
        id: buttons
        height: parent.height * 0.2
        color: "#00000000"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0

        Rectangle {
            id: apply
            width: parent.width * 0.5
            color: mouseArea1.containsMouse ? "#99ffffff" : "#99bada55"
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0

            Text {
                id: text1
                color: "#ededed"
                text: qsTr("Apply") + gameAdapter.translation
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.verticalCenter: parent.verticalCenter
                anchors.fill: parent
                font.pixelSize: 22
                font.bold: true
            }

            MouseArea {
                id: mouseArea1
                hoverEnabled: true
                anchors.fill: parent
                onClicked: {
                    applySettings();
                }
                onContainsMouseChanged: {
                    if(mouseArea1.containsMouse) gameAdapter.slt_playSound(Sound.Select2);
                }
            }
        }

        Rectangle {
            id: cancel
            width: parent.width * 0.5
            color: mouseArea2.containsMouse ? "#99ffffff" : "#99d14141"
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.bottom: parent.bottom
            Text {
                id: text2
                color: "#ededed"
                text: qsTr("Cancel") + gameAdapter.translation
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 22
                verticalAlignment: Text.AlignVCenter
                font.bold: true
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
            }

            MouseArea {
                id: mouseArea2
                hoverEnabled: true
                anchors.fill: parent
                onClicked: {
                    discardSettings();
                    gameAdapter.slt_playSound(Sound.Select3);
                }
                onContainsMouseChanged: {
                    if(mouseArea2.containsMouse) gameAdapter.slt_playSound(Sound.Select2);
                }
            }
            anchors.topMargin: 0
            anchors.top: parent.top
            anchors.bottomMargin: 0
        }
    }


}
