import QtQuick 2.0
import CardTypes 1.0

Item {
    id: playerplayed
    property alias msg: msg

    anchors.right: parent.right
    anchors.rightMargin: 120
    anchors.left: parent.left
    anchors.leftMargin: 120
    anchors.bottom: parent.bottom
    anchors.bottomMargin: 120
    anchors.top: parent.top
    anchors.topMargin: 120

    /*
        \qmlmethod createCard(card)

        This method creates a new card to show in this form based on the parameter given
    */
    function createCard(card) {
        var source = "";

        switch(card) {
            case CardType.Defuse:
                source = "qrc:///defuseCard"
                break;
            case CardType.Attack:
                source = "qrc:///attackCard";
                break;
            case CardType.Favor:
                source = "qrc:///favorCard";
                break;
            case CardType.Nope:
                source = "qrc:///nopeCard";
                break;
            case CardType.Skip:
                source = "qrc:///skipCard";
                break;
            case CardType.Shuffle:
                source = "qrc:///shuffleCard";
                break;
            case CardType.SeeTheFuture:
                source = "qrc:///seeTheFutureCard";
                break;
            case CardType.BikiniCat:
                source = "qrc:///bikiniCatCard";
                break;
            case CardType.CatsSchroedinger:
                source = "qrc:///catsSchroedingerCard";
                break;
            case CardType.ShyBladder:
                source = "qrc:///shyBladderCatCard";
                break;
            case CardType.ZombieCat:
                source = "qrc:///zombieCatCard";
                break;
            case CardType.MommaCat:
                source = "qrc:///mommaCatCard";
                break;
            case CardType.ExplosiveKitten:
                source = "qrc:///ExplodingKittenCard"
                break;
            default:
                break;
        }

        var component = Qt.createComponent("qrc:///Card.qml");
        var card = component.createObject(row2, {"width": "180", "height": row2.height, "anchors.verticalCenter": "parent.verticalCenter", "countRect.visible": false,"image.source": source});

        if(card == null) {console.log("Card not created")}
        if( component.status == Component.Error )
                console.debug("Error:"+ component.errorString() );
    }

    Rectangle {
        id: background
        color: "#DD000000"
        anchors.fill: parent

        MouseArea {
            id: mouseBlock
            anchors.fill: parent
        }

        Text {
            id: msg
            color: "#ffffff"
            text: qsTr("Text") + gameAdapter.translation
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 24
        }


        Rectangle {
            id: exitrect
            x: 0
            width: 40
            height: 40
            color: "#99ffffff"
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0

            Text {
                id: exittext
                color: "#d14141"
                text: "X"
                font.bold: true
                anchors.fill: parent
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 22
            }

            MouseArea {
                id: clickArea
                anchors.fill: parent
                onClicked: playerplayed.destroy()
            }
        }


        Flickable {
            id: flickable1
            width: parent.width
            height: parent.height
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.topMargin: 0
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: msg.bottom
            contentWidth: row2.width
            visible: true
            interactive: true
            contentHeight: parent.height
            boundsBehavior: Flickable.DragAndOvershootBounds
            flickableDirection: Flickable.HorizontalFlick

            Row {
                id: row2
                width: childrenRect.width
                height: parent.height
                spacing: 20
                anchors.left: parent.left
                anchors.leftMargin: 0
            }
        }


    }


}
