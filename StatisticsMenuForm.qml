import QtQuick 2.0

Item {
    id: item1

    property alias stats_from: stats_from
    property alias level: level
    property alias exp_until_next: exp_until_next
    property alias cards_played: cards_played1
    property alias wins: wins1
    property alias lfoes_content: lfoes_content
    property alias losses: losses1
    property alias defused: defused1
    property alias nopes: nopes1
    property alias special: special1

    Column {
        id: types
        anchors.left: parent.left
        anchors.top: exp.bottom
        spacing: 10
        width: parent.width / 2
        height: childrenRect.height
        anchors.topMargin: 10

        Text {
            id: cards_played
            text: qsTr("cards played") + gameAdapter.translation
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
        }

        Text {
            id: wins
            text: qsTr("wins") + gameAdapter.translation
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
        }

        Text {
            id: losses
            text: qsTr("losses") + gameAdapter.translation
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
        }

        Text {
            id: defused
            text: qsTr("times defused") + gameAdapter.translation
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
        }

        Text {
            id: nopes
            text: qsTr("times noped") + gameAdapter.translation
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
        }

        Text {
            id: special
            text: qsTr("special actions") + gameAdapter.translation
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
        }

        Text {
            id: lfoes
            text: qsTr("last played againts:") + gameAdapter.translation
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
        }
    }

    Column {
        id: contents
        width: parent.width / 2
        height: types.height
        spacing: 10
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.top: exp.bottom
        anchors.topMargin: 10

        Text {
            id: cards_played1
            text: "cards played"
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
        }

        Text {
            id: wins1
            text: "wins1"
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
        }

        Text {
            id: losses1
            text: "losses1"
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
        }

        Text {
            id: defused1
            text: "times defused1"
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
        }

        Text {
            id: nopes1
            text: "times noped"
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
        }

        Text {
            id: special1
            text: "special1 actions"
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
        }
    }

    Text {
        id: stats_from
        height: 20
        text: ""
        font.bold: true
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        font.pixelSize: 22
    }

    Rectangle {
        id: exp
        height: childrenRect.height
        color: "#99000000"
        anchors.top: stats_from.bottom
        anchors.topMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0

        Text {
            id: level
            height: 20
            color: "#ededed"
            text: ""
            anchors.rightMargin: 0
            anchors.right: parent.right
            font.pixelSize: 20
            anchors.leftMargin: 0
            anchors.left: parent.left
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }

        Text {
            id: exp_until_next
            height: 20
            color: "#ededed"
            text:""
            anchors.rightMargin: 0
            anchors.right: parent.right
            font.pixelSize: 20
            anchors.top: level.bottom
            anchors.leftMargin: 0
            anchors.left: parent.left
            anchors.topMargin: 10
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Text {
        id: lfoes_content
        height: 30
        text: ""
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.top: types.bottom
        anchors.topMargin: 20
        wrapMode: Text.WordWrap
        font.pixelSize: 12
    }

}
