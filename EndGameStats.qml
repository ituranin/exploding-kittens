import QtQuick 2.0
import CardTypes 1.0

Item {
    id: endgamestats
    property alias msg: msg

    anchors.right: parent.right
    anchors.rightMargin: 120
    anchors.left: parent.left
    anchors.leftMargin: 120
    anchors.bottom: parent.bottom
    anchors.bottomMargin: 120
    anchors.top: parent.top
    anchors.topMargin: 120

    /*
        \qmlmethod loadStats(gameStats)

        This method sets the counts of all stats received in gameStats array
    */
    function loadStats(gameStats) {
        if(gameStats != null) {
            mostCardsName.text = gameStats[0];
            mostCardsCount.text = gameStats[1];
            mostDefusesName.text = gameStats[2];
            mostDefusesCount.text = gameStats[3];
            mostNopesName.text = gameStats[4];
            mostNopesCount.text = gameStats[5];
            mostSpecialName.text = gameStats[6];
            mostSpecialCount.text = gameStats[7];
        }
    }

    Rectangle {
        id: background
        color: "#DD000000"
        anchors.fill: parent

        MouseArea {
            id: mouseBlock
            anchors.fill: parent
        }

        Text {
            id: msg
            color: "#ededed"
            text: qsTr("Game Statistics") + gameAdapter.translation
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 24
        }


        Rectangle {
            id: content
            color: "#00000000"
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.top: msg.bottom
            anchors.topMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0

            Rectangle {
                id: mostCards
                width: parent.width * 0.25
                height: childrenRect.height
                color: "#00000000"
                anchors.verticalCenterOffset: 0
                anchors.verticalCenter: parent.verticalCenter

                Text {
                    id: type
                    color: "#ededed"
                    text: qsTr("Addicted")
                    wrapMode: Text.WordWrap
                    font.bold: true
                    anchors.top: parent.top
                    anchors.topMargin: 0
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    font.pixelSize: 22
                }

                Text {
                    id: mostCardsName
                    color: "#ededed"
                    text: qsTr("PLAYER")
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.top: type.bottom
                    anchors.topMargin: 0
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    font.pixelSize: 22
                }

                Text {
                    id: mostCardsCount
                    color: "#ededed"
                    text: "0"
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    anchors.top: mostCardsName.bottom
                    anchors.topMargin: 0
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.pixelSize: 18
                }
            }

            Rectangle {
                id: mostDefuses
                y: 1
                width: parent.width * 0.25
                height: childrenRect.height
                color: "#00000000"
                anchors.left: mostCards.right
                anchors.leftMargin: 0
                anchors.verticalCenterOffset: 0
                anchors.verticalCenter: parent.verticalCenter
                Text {
                    id: type1
                    color: "#ededed"
                    text: qsTr("Bomb Squad")
                    wrapMode: Text.WordWrap
                    verticalAlignment: Text.AlignVCenter
                    anchors.right: parent.right
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.leftMargin: 0
                    anchors.rightMargin: 0
                    anchors.topMargin: 0
                    font.bold: true
                    font.pixelSize: 22
                    horizontalAlignment: Text.AlignHCenter
                }

                Text {
                    id: mostDefusesName
                    color: "#ededed"
                    text: qsTr("PLAYER")
                    verticalAlignment: Text.AlignVCenter
                    anchors.right: parent.right
                    anchors.left: parent.left
                    anchors.top: type1.bottom
                    anchors.leftMargin: 0
                    anchors.rightMargin: 0
                    anchors.topMargin: 0
                    font.pixelSize: 22
                    horizontalAlignment: Text.AlignHCenter
                }

                Text {
                    id: mostDefusesCount
                    color: "#ededed"
                    text: "0"
                    verticalAlignment: Text.AlignVCenter
                    anchors.right: parent.right
                    anchors.top: mostDefusesName.bottom
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    anchors.rightMargin: 0
                    anchors.topMargin: 0
                    font.pixelSize: 18
                    horizontalAlignment: Text.AlignHCenter
                }
            }

            Rectangle {
                id: mostNopes
                x: 0
                y: 199
                width: parent.width * 0.25
                height: childrenRect.height
                color: "#00000000"
                anchors.verticalCenterOffset: 0
                anchors.left: mostDefuses.right
                anchors.leftMargin: 0
                anchors.verticalCenter: parent.verticalCenter
                Text {
                    id: type2
                    color: "#ededed"
                    text: qsTr("Nay Sayer")
                    wrapMode: Text.WordWrap
                    verticalAlignment: Text.AlignVCenter
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.rightMargin: 0
                    anchors.leftMargin: 0
                    font.bold: true
                    anchors.topMargin: 0
                    font.pixelSize: 22
                    horizontalAlignment: Text.AlignHCenter
                }

                Text {
                    id: mostNopesName
                    color: "#ededed"
                    text: qsTr("PLAYER")
                    verticalAlignment: Text.AlignVCenter
                    anchors.right: parent.right
                    anchors.top: type2.bottom
                    anchors.left: parent.left
                    anchors.rightMargin: 0
                    anchors.leftMargin: 0
                    anchors.topMargin: 0
                    font.pixelSize: 22
                    horizontalAlignment: Text.AlignHCenter
                }

                Text {
                    id: mostNopesCount
                    color: "#ededed"
                    text: "0"
                    verticalAlignment: Text.AlignVCenter
                    anchors.right: parent.right
                    anchors.left: parent.left
                    anchors.top: mostNopesName.bottom
                    anchors.rightMargin: 0
                    anchors.leftMargin: 0
                    anchors.topMargin: 0
                    font.pixelSize: 18
                    horizontalAlignment: Text.AlignHCenter
                }
            }

            Rectangle {
                id: mostSpecial
                x: 8
                y: 206
                width: parent.width * 0.25
                height: childrenRect.height
                color: "#00000000"
                anchors.verticalCenterOffset: 0
                anchors.left: mostNopes.right
                anchors.leftMargin: 0
                anchors.verticalCenter: parent.verticalCenter
                Text {
                    id: type3
                    color: "#ededed"
                    text: qsTr("'Special'")
                    wrapMode: Text.WordWrap
                    verticalAlignment: Text.AlignVCenter
                    anchors.right: parent.right
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.leftMargin: 0
                    anchors.rightMargin: 0
                    anchors.topMargin: 0
                    font.bold: true
                    font.pixelSize: 22
                    horizontalAlignment: Text.AlignHCenter
                }

                Text {
                    id: mostSpecialName
                    color: "#ededed"
                    text: qsTr("PLAYER")
                    verticalAlignment: Text.AlignVCenter
                    anchors.right: parent.right
                    anchors.left: parent.left
                    anchors.top: type3.bottom
                    anchors.leftMargin: 0
                    anchors.rightMargin: 0
                    anchors.topMargin: 0
                    font.pixelSize: 22
                    horizontalAlignment: Text.AlignHCenter
                }

                Text {
                    id: mostSpecialCount
                    color: "#ededed"
                    text: "0"
                    verticalAlignment: Text.AlignVCenter
                    anchors.right: parent.right
                    anchors.top: mostSpecialName.bottom
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    anchors.rightMargin: 0
                    anchors.topMargin: 0
                    font.pixelSize: 18
                    horizontalAlignment: Text.AlignHCenter
                }
            }
        }

        Rectangle {
            id: exitrect
            x: 0
            width: 40
            height: 40
            color: "#99ffffff"
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0

            Text {
                id: exittext
                color: "#d14141"
                text: "X"
                anchors.fill: parent
                font.bold: true
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 22
            }

            MouseArea {
                id: clickArea
                anchors.fill: parent
                onClicked: endgamestats.destroy()
            }
        }


    }


}
