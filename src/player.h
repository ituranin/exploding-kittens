#ifndef PLAYER_H
#define PLAYER_H
#include <qobject.h>
#include <string>
#include "cards.h"
#include "deck.h"
#include "logging.h"

/**
 * @brief The publicPlayer class holds non sensitive information about a player for transmitting to the clients
 */
class publicPlayer : public QObject{
    Q_OBJECT

    /**
     * @brief playerName qproperty for GUI
     */
    Q_PROPERTY(QString playerName READ getPlayerName WRITE setPlayerName NOTIFY playerNameChanged)

public:

    /**
     * @brief publicPlayer constructor
     * @param playerName its name
     * @param playerIPAdress its ipAddress
     */
    publicPlayer(QString playerName, QString playerIPAdress);

    /**
     * @brief operator ==
     * @param p the comparator
     * @return true or false
     */
    bool operator== (const publicPlayer* p);

    /**
     * @brief operator !=
     * @param p the comparator
     * @return true or false
     */
    bool operator!= (const publicPlayer* p);

    /**
     * @brief hasLost to evaluate if the player has allready lost (for GUI reasons)
     */
    bool hasLost = false;

    /**
     * @brief getPlayerName returns the name of the player
     * @return name of the player
     */
    QString getPlayerName() const;

    /**
     * @brief setPlayerName sets the name of the player
     * @param name in which the name should be changed
     */
    void setPlayerName(const QString &name);

    /**
     * @brief getIPAddress returns the IPADress of a player
     * @return QString holding the Address
     */
    QString getIPAddress() const;

    /**
     * @brief setIPAddress sets the address of a player
     * @param ipAdress the address of the player
     */
    void setIPAddress(QString ipAdress);

    /**
     * @brief isReady returns of a player is ready
     * @return bool
     */
    bool isReady() const;

    /**
     * @brief setAmountOfCardsOnHand sets the card amount the player has
     * @param value of cards
     */
    void setAmountOfCardsOnHand(int value);

    /**
     * @brief getAmountOfCardsOnHand returns the amount of cards the player has on its hand
     * @return amount of cards
     */
    int getAmountOfCardsOnHand() const;

    /**
     * @brief getPlayerLost returns if a player has lost
     * @return
     */
    bool getPlayerLost() const;


signals:
    /**
     * @brief playerNameChanged is emitted when a player changes its name
     */
    void playerNameChanged();

    /**
     * @brief playerReadyChanged is emitted when is ready changed
     */
    void playerReadyChanged();

protected:

    /**
     * @brief m_playerName the players public name
     */
    QString m_playerName;

    /**
     * @brief m_playerIPAdress the players IPAdress for transmitting information. Its used to differentiate between the players
     */
    QString m_playerIPAdress;

    /**
     * @brief m_isReady is set on the client side to check if the game can start
     */
    bool m_isReady = false;

    /**
     * @brief amountOfCardsOnHand for GUI reasons to see how many cards one has on its hand
     */
    int amountOfCardsOnHand = 0;
};




/**
 * @brief The privatePlayer class is at the host only and used in the gameplay
 */
class privatePlayer : public publicPlayer{

private:

    /**
     * @brief playerLogging the logfile every player has
     */
    Logging playerLogging;


public:
    using publicPlayer::publicPlayer;
    ~privatePlayer();

    /**
     * @brief playerDeck its player Deck which is cross checked on the server side
     */
    PlayerDeck* playerDeck = nullptr;

    /**
     * @brief setPlayerReady switches ready between true and false when a player sets itself to
     */
    void setPlayerReady();

    /**
     * @brief assignDeck assigns a player deck to a player
     * @param deck the deck of cards
     */
    void assignDeck(PlayerDeck* deck);

    /**
     * @brief setPlayerLost sets a player to lost
     */
    void setPlayerLost();

    /**
     * @brief logPlayedCard adds a card played to the players log
     * @param card which was played
     */
    void logPlayedCard(AbstractCard::CardTypes card);

    /**
     * @brief setLoggingNopingToFinished sets the nioping to finished in the logfile
     */
    void setLoggingNopingToFinished();

    /**
     * @brief getReport returns the logfile created for a player during a game
     * @return QMAp containing the bestOf logs
     */
    QMap<Logging::logVals, unsigned int> getReport() const;
};

#endif // PLAYER_H
