#include "gameadapter.h"


gameAdapter::gameAdapter():m_connectToIpAdressText("127.0.0.1"){
    localPublicPlayerList = new std::vector<publicPlayer*>();   //for sending updated playerlists between server & client
    publicPlayerListBuffer = new std::vector<publicPlayer*>();

    //create attending playerlist
    hostPrivatePlayerList = new std::vector<privatePlayer*>();        //created on heap to pass it into gamePlay later on

    //read name from DataBase
    qDebug() << "[GA]" << QSqlDatabase::database().databaseName();
    dbHelper = DBHelper::instance();
    qDebug() << "[GA]" << dbHelper;
    qDebug() << "[GA]" << QSqlDatabase::database().databaseName();

    translator = new QTranslator();
    qDebug() << "[TRANSLATOR] ######## " << translator->load(":/de.qm");

    //create audio helper
    audio = new Audio();

    localPlayer = new privatePlayer(getActiveProfile(), "");
    PlayerDeck* playerDeck = new PlayerDeck();
    localPlayer->assignDeck(playerDeck);

    //create client
    client = new Client();

    //connect Signals FROM network
    bool cl1 = connect(client, SIGNAL(sig_connected()), this, SLOT(slt_clientConnected())); //checked
    qDebug() << "[GA] cl1 ?" << cl1;

    bool cl2 = connect(client, SIGNAL(sig_disconnected()), this, SLOT(slt_clientDisconnected()));   //connected
    qDebug() << "[GA] cl2 ?" << cl2;

    //connect client NW signal to this List. To have List in Lobby
    bool nw1 = connect(client, SIGNAL(sig_receivedNewPlayerlist(std::vector<publicPlayer*>*)), this, SLOT(slt_receivedNewPlayerlist(std::vector<publicPlayer*>*))); //checekd
    qDebug() << "nw1 ?" << nw1;

    //connect signal game is starting from client to this
    bool gmrdy = connect (client, SIGNAL(gameIsStarting()), this, SLOT(slt_networkReceivedGameStarting())); //checked
    qDebug() << "gmrdy ?" << gmrdy;

    connect(client, SIGNAL(ipReceived(QString)), this, SLOT(slt_clientReceivedIP(QString)));

    //signal connections
    connect(client, SIGNAL(sig_errorMessage(QString)), this, SIGNAL(sig_errorMessage(QString)));

    connect(client,SIGNAL(activePlayerReceived(QString)), this, SLOT(slt_networkReceivedActivePlayer(QString)));

    connect(client, SIGNAL(sig_receivedCardAmount(QString,uint)), this, SLOT(slt_receivedNewCoPlayerCardAmount(QString,uint))); //checked

    connect(client, SIGNAL(sig_receivedDrawPileCount(uint)), this, SLOT(slt_receivedNewAmountForDrawPile(uint)));   //checked

    connect(client, SIGNAL(sig_receivedCards(std::vector<AbstractCard::CardTypes>)), this, SLOT(slt_cardsReceivedForPlayerHandCards(std::vector<AbstractCard::CardTypes>)));

    connect(client, SIGNAL(playedCardsReceived(QString,std::vector<AbstractCard::CardTypes>)), this, SLOT(slt_playedCardsReceived(QString,std::vector<AbstractCard::CardTypes>)));

    connect(client, SIGNAL(sig_newStateReceived(BaseState::playStates)), this, SLOT(slt_networkChangedState(BaseState::playStates)));

    connect(client, SIGNAL(sig_receivedPlayerStats(QMap<int,uint>,QString)), this, SLOT(slt_receivedPlayerStats(QMap<int,uint>,QString)));

    connect(client, SIGNAL(sig_receivedGameStats(QMap<int,QMap<QString,uint> >)), this, SLOT(slt_receivedGameStats(QMap<int,QMap<QString,uint> >)));

    connect(client, SIGNAL(playerLost(QString)), this, SIGNAL(sig_playerLost(QString)));

    connect(client, SIGNAL(sig_progressChangedReceived(double)), this, SLOT(slt_progressChanged(double)));

    connect(client, SIGNAL(playerWon(QString)), this, SLOT(slt_playerWon(QString)));

    connect(client, SIGNAL(sig_shuffleReceived()), this, SLOT(slt_shuffleReceived()));

    connect(client, SIGNAL(sig_receivedSeeTheFutureCards(std::vector<AbstractCard::CardTypes>)), this, SLOT(slt_seeTheFutureCardsReceived(std::vector<AbstractCard::CardTypes>)));

    //connect TO network

    connect(this, SIGNAL(sig_sendServerKittenPositionInDrawPile(int)), client, SLOT(slt_sendServerKittenPositionInDrawPile(int)));

    bool cl3 = connect(this, SIGNAL(sig_SendCardsPlayedToNetwork(std::vector<AbstractCard::CardTypes>)), client, SLOT(playCards(std::vector<AbstractCard::CardTypes>)));   //checked
    qDebug() << "[GA] cl3 ?" << cl3;

    connect(this, SIGNAL(sig_playerLost(QString)), this, SLOT(slt_playerLost(QString)));


    //create handDeckGUI blubb

    //dbHelper->ddddropTheTable();

}

gameAdapter::~gameAdapter()
{
    delete hostPrivatePlayerList;
    delete localPublicPlayerList;
    delete localPlayer;
    delete dbHelper;
    delete server;
    delete client;
    delete translator;
    delete publicPlayerListBuffer;
}

void gameAdapter::selectLanguage(int lang)
{
    qDebug() << "[TRANSLATOR] selectLanguage called " << lang;
    switch (lang) {
    case 0:
        qApp->removeTranslator(translator);
        break;
    case 1:
        qApp->installTranslator(translator);
        break;
    default:
        break;
    }

    emit languageChanged();
}

QString gameAdapter::getIpText() const{
    return m_connectToIpAdressText;
}

void gameAdapter::setIpText(const QString &ipText){
    //more checks here for ip triples only beeing 0 to 254 (maybe no int ?)
    if (ipText != m_connectToIpAdressText) {
        m_connectToIpAdressText = ipText;
        emit ipAdressInTextboxChanged();
        qDebug() << "[GA] IP Adress changed to : " << m_connectToIpAdressText;
    }
}

QVector<AbstractCard::CardTypes> gameAdapter::getPlayerHandCards()
{

    //clear Maps
    m_playerHandCardsForGUI.clear();

    //fill map with playerCards
    localPlayer->playerDeck->readCards(m_playerHandCardsForGUI);

    return m_playerHandCardsForGUI;
}

unsigned int gameAdapter::getAmountOfCardsInDrawPile() const
{
    return m_amountOfCardsInDrawPile;
}

void gameAdapter::setAmountOfCardsInDrawPile(unsigned int amountOfCardsInDrawPile){
qDebug() << "[GA] set amount in drawPile";
    m_amountOfCardsInDrawPile = amountOfCardsInDrawPile;
    emit amountOfCardsInDrawPileChanged();
}

int gameAdapter::getAmountCards(AbstractCard::CardTypes type)
{
//    qDebug() << "[GA] getting amount of " << type;

    //cheap check, only do this if the game started so all is properly created
    if (m_matchIsRunning){
        return localPlayer->playerDeck->getAmountOfCards(type);
    }
    else{
//        qDebug() << "[GA] no game in progress, no cards";
        return -1;
    }
}

int gameAdapter::getAmountAttackCards()
{
    return getAmountCards(AbstractCard::Attack);
}

int gameAdapter::getAmountDefuseCards()
{
    return getAmountCards(AbstractCard::Defuse);
}

int gameAdapter::getAmountExplosiveKittenCards()
{
    return getAmountCards(AbstractCard::ExplosiveKitten);
}

int gameAdapter::getAmountFavorCards()
{
    return getAmountCards(AbstractCard::Favor);
}

int gameAdapter::getAmountNopeCards()
{
    return getAmountCards(AbstractCard::Nope);
}

int gameAdapter::getAmountSeeTheFutureCards()
{
    return getAmountCards(AbstractCard::SeeTheFuture);
}

int gameAdapter::getAmountShuffleCards()
{
    return getAmountCards(AbstractCard::Shuffle);
}

int gameAdapter::getAmountSkipCards()
{
    return getAmountCards(AbstractCard::Skip);
}

int gameAdapter::getAmountBikiniCatCards()
{
    return getAmountCards(AbstractCard::BikiniCat);
}

int gameAdapter::getAmountCatsSchroedingerCards()
{
    return getAmountCards(AbstractCard::CatsSchroedinger);
}

int gameAdapter::getAmountMommaCatCards()
{
    return getAmountCards(AbstractCard::MommaCat);
}

int gameAdapter::getAmountShyBladderCards()
{
    return getAmountCards(AbstractCard::ShyBladder);
}

int gameAdapter::getAmountZombieCatCards()
{
    return getAmountCards(AbstractCard::ZombieCat);
}


//callable from UI
void gameAdapter::hostGame(){

if (!m_isHost){
    m_isHost = true;
    //if hosting start network Server:

    if (server == NULL){
        qDebug() << "Server NULL, creating";
        server = new Server();      //remember to delete !

        //connect server Slots:

        //new player from network:
        bool nw1 = connect(server, SIGNAL(addPlayer(QString,QString)),this,SLOT(slt_newPlayerConnectedToHost(QString,QString)));    //checked
        qDebug() << "[GA] nw1 ?" << nw1;

        //updated player list to network
        bool nw2 = connect(this, SIGNAL(sigSendUpdatedPlayerList(std::vector<publicPlayer*>*)),server,SLOT(sendPlayerList(std::vector<publicPlayer*>*)));   //checked
        qDebug() << "[GA] nw2 ?" << nw2;

        //player set ready
        //todo
        //bool nw3 = connect(server, SIGNAL(playerUpdatedReady(Qstring)),this,SLOT(sltCheckPlayerReady(QString)));
        //qDebug() << "nw3 ?" << nw3;

        //start Game
        bool nw4 = connect(this, SIGNAL(sig_StartHostedGame()),server,SLOT(sendGameIsStarting()));  //checked
        qDebug() << "[GA] nw4 ?" << nw4;

        bool nw5 = connect(server, SIGNAL(sig_playerDisconnectedFromGame(QString)), this, SLOT(slt_clientDisconnectedReceived(QString)));

    }
    else{
        qDebug() << "[GA] Server allready running";
    }

    //join game
    //set connecting ip to local
    m_connectToIpAdressText = "127.0.0.1";
    joinGame();
}
else{
    qDebug() << "[GA] a game is allready open";
}
}

//callable from UI
void gameAdapter::joinGame(){

    qDebug() << "[GA] Joining game on : " << m_connectToIpAdressText;


    //read localIpAdress from ClientNetwork
    //add it to local Player
    localPlayer->setIPAddress("::ffff:127.0.0.1");

    //establish network connection
    client->establish(m_connectToIpAdressText);
}

//void gameAdapter::lobby(){

//    //show lobby screen
//    //emit switchToLobbyScreen(); //qporperty which refreshs playerView in Lobby Screen
//}

void gameAdapter::startHostedGame(){

    //checke that server is running (may be deleted after game before

    if((!m_gameStarted) && (m_isHost)){

        if (hostPrivatePlayerList->size() > 1){

            if (server != NULL){

                m_gameStarted = true;

                //check that all players are ready
                qDebug() << "[GA] starting hosted game";
                qDebug() << "[GA] Players: " << hostPrivatePlayerList->size();
                for (auto iter = hostPrivatePlayerList->begin(); iter != hostPrivatePlayerList->end(); ++iter) {
                    qDebug() << "[GA] Name: " << (*iter)->getPlayerName();
                }


                //create a new gameplay()
                hostgp = new hostGameplay(hostPrivatePlayerList);
                hostgp->assignServer(server);

                //Signal for if game has ended
                bool gmpToGa = connect(hostgp, SIGNAL(gameEndedSetNewGameReady()), this, SLOT(gameHasEnded())); //checked
                qDebug() << "[GA] connection gamepla to adapter ? " << gmpToGa;


                //Signal if a player leaves ingame
                bool playerLeavesTOGP = connect (this, SIGNAL(sig_informGameplayPlayerLeftGame(QString)), hostgp, SLOT(slt_playerLeftGame(QString)));   //checked
                qDebug() << "[GA] connection playerLeavesTOGP ?" << playerLeavesTOGP;


                //send network that game is about to start
                emit sig_StartHostedGame();

                hostgp->start();    //connects the signals to network too
                //deactivate start game Button !
            }
            else
            {
                qDebug() << "[GA] Server not Running, cant start game !";
            }
        }else
        {
            qDebug() << "[GA] must be at least 2 players to start a game !";
        }
    }
    else{
        qDebug() << "[GA] game allready in progress";
    }

}

void gameAdapter::startJoinedGame(){
    //check that client is running maybe deleted after game bevore

    qDebug() << "[GA] starting joined game";


    qDebug() << "[GA] Players: " << hostPrivatePlayerList->size();
    for (int i = 0; i < hostPrivatePlayerList->size(); ++i) {
        qDebug() << hostPrivatePlayerList->at(i)->getPlayerName();
    }

//        connect (client, SIGNAL(sig_shuffleReceived()), this, SLOT(slt_shuffleReceived()));
//    client / sig_shuffleReceived;




    //emit signal to gui to show gameform
    emit sig_createGameForm();
}

void gameAdapter::cleanUp(){

//    disconnect(this, 0, server, 0);
//    disconnect(server, 0, this, 0);


    //game ended so cleanUp the Game and do other Stuff like statistics

    //todo clalculate statistics
    qDebug() << "[GA] call cleanup:";

    //just dont ... put it to the destructor
    //qDebug() << "try to delete: " << attendingPlayer;
    //delete hostPrivatePlayerList; //implicitly deletes all its containings
    qDebug() << "[GA] delete hostgp:";

    qDebug() << "[DISCONNECTING HOSTGP]" << disconnect(hostgp, 0, 0, 0);
    qDebug() << "[DISCONNECTING HOSTGP]" << disconnect(this, 0, hostgp, 0);
    delete hostgp;

    qDebug() << "[DISCONNECTING SERVER]" << disconnect(server, 0, 0, 0);
    qDebug() << "[DISCONNECTING SERVER]" << disconnect(this, 0, server, 0);
    server->deleteLater();
    server = NULL;

    m_isHost = false;
    //remove all players
//    if(hostPrivatePlayerList->size() > 0){
//        for (auto iter = hostPrivatePlayerList->begin(); iter != hostPrivatePlayerList->end(); ++iter){
//            slt_kickPlayer((*iter)->getIPAddress());
//        }
//    }

    //clear list
    qDebug() << "[GA] clear lists:";
    hostPrivatePlayerList->clear();
    publicPlayerListBuffer->clear();
    localPublicPlayerList->clear();

    /*qDebug() << "[DISCONNECTING SERVER]" << disconnect(server, 0, 0, 0);
    qDebug() << "[DISCONNECTING SERVER]" << disconnect(this, 0, server, 0);
    delete server;*/

    //delete server;

//    delete server; -> deletes self on empty list

//    if (server != NULL){
//        qDebug() << "try to delete server";
//        delete server;
//    }


//    if (client != NULL){
//        qDebug() << "try to delete client";
//        delete client;
//    }
}

void gameAdapter::assignNewLocalPublicPlayerList(std::vector<publicPlayer *> *newLocalPlayerList)
{
    //TODO
    //exchange localPlayerList with new List

    std::vector<publicPlayer *> *tempPlayerList = new std::vector<publicPlayer*>();

    qDebug() << "[GA] assign new list with size: " << newLocalPlayerList->size();
    qDebug() << "[GA] address tempList(empty)" << tempPlayerList;
    qDebug() << "[GA] address newLocalList" << newLocalPlayerList;
    qDebug() << "[GA] address oldLOcalList" << localPublicPlayerList;

    tempPlayerList = localPublicPlayerList;
    localPublicPlayerList = newLocalPlayerList;

    newLocalPlayerList = NULL;

    qDebug() << "[GA] after assign new list:";
    qDebug() << "[GA] address tempList(oldLocalList)" << tempPlayerList;
    qDebug() << "[GA] address newLocalList" << newLocalPlayerList;
    qDebug() << "[GA] address oldLocalList(newLocalPlayerlist)" << localPublicPlayerList;


    delete tempPlayerList;
    delete newLocalPlayerList;
}

void gameAdapter::removeFromPlayerList(QString ip)
{
    for(auto iter = hostPrivatePlayerList->begin(); iter != hostPrivatePlayerList->end();){

        //1. find player by ip
        if((*iter)->getIPAddress() == ip){

            //2. notify GUI
            emit sig_playerLeftLobby((*iter)->getPlayerName(), (*iter)->getIPAddress());

            //3. remove from playerlist
            iter = hostPrivatePlayerList->erase(iter);

            //4. send new List to clients
            sendUpdatedPlayerList();

            //leave loop
            break;

        }else
             ++iter;
    }
}


void gameAdapter::sendUpdatedPlayerList(){

    if (m_isHost){

        qDebug() << "[GA] Host creates new playerlist:";

        //clear Buffer

        publicPlayerListBuffer->clear();
        //push back all new players

        for(auto iter = hostPrivatePlayerList->begin(); iter != hostPrivatePlayerList->end(); ++iter){
            //attendingPublicPlayer->at(i) = (*iter);
            publicPlayerListBuffer->push_back(*iter);
        }

        //send them to network
        emit sigSendUpdatedPlayerList(publicPlayerListBuffer); //order to network to send refreshed List to others
        qDebug() << "[GA] Host sends new playerlist:";
    }
}


void gameAdapter::playCards()
{
    //check the buffer
    if (cardsToBePlayedBuffer.size() > 0){
        emit sig_SendCardsPlayedToNetwork(cardsToBePlayedBuffer);

        if(activeState == BaseState::playCardState){
            qDebug() << "state is: " << activeState;
            m_endedPlayCardState = true;
            emit sig_localPlayersTurnEnded();
        }
    }
    else{
        if(!m_endedPlayCardState){
            emit sig_SendCardsPlayedToNetwork(cardsToBePlayedBuffer);

            qDebug() << "[GA - playCards] - buffer empty, no cards played - switching state to draw card";
            m_endedPlayCardState = true;
            emit sig_localPlayersTurnEnded();

        }
    }
    //erase cards from buffer
    cardsToBePlayedBuffer.clear();
}

QString gameAdapter::getActiveProfile()
{
    return dbHelper->getActiveProfile();
}

void gameAdapter::changePlayerName(QString name)
{
    dbHelper->changeName(name);
}

QVariantList gameAdapter::getActiveProfileStats()
{
    return dbHelper->getActivePlayerStats();
}

bool gameAdapter::createLocalProfile(QString name)
{
    localPlayer->setPlayerName(name);
    return dbHelper->createProfile(name);
}

void gameAdapter::clientLeaveGame()
{
    qDebug() << "[GA] clientLeaveGame called";
    dbHelper->updateStats(0,0,0,0,0,"n/a");
    client->socket.disconnectFromHost();
}


//****************************
//***********slots************
//****************************

void gameAdapter::gameHasEnded(){
    //signal ended received
    qDebug() << "[GA] Signal game ended received";
    cleanUp();
    qDebug() << "[GA] ready to start new game";
    m_gameStarted = !m_gameStarted;
}

void gameAdapter::refreshGUIHandCards()
{
//    qDebug() << "[GA] its playersTurn, refresh handCards in GUI";
    emit qmlAmountAttackCardsChanged();
    emit qmlAmountDefuseCardsChanged();
    emit qmlAmountExplosiveKittenCardsChanged();
    emit qmlAmountFavorCardsChanged();
    emit qmlAmountNopeCardsChanged();
    emit qmlAmountSeeTheFutureCardsChanged();
    emit qmlAmountShuffleCardsChanged();
    emit qmlAmountSkipCardsChanged();
    emit qmlAmountBikiniCatCardsChanged();
    emit qmlAmountCatsSchroedingerCardsChanged();
    emit qmlAmountMommaCatCardsChanged();
    emit qmlAmountShyBladderCardsChanged();
    emit qmlAmountZombieCatCardsChanged();
}


void gameAdapter::slt_newPlayerConnectedToHost(QString name, QString ip){
    //create new player and add to list

    privatePlayer* newPlayer = new privatePlayer(name, ip);
    newPlayer->setPlayerReady();
    hostPrivatePlayerList->push_back(newPlayer);
    qDebug() << "[GA] added new joined player to Server privateList: " << hostPrivatePlayerList->back()->getPlayerName() << " with ip: " << hostPrivatePlayerList->back()->getIPAddress();

    sendUpdatedPlayerList();
}


void gameAdapter::sltCheckPlayerReady(QString ip){
    //new ready signal from player

    //check for ip in list and set player ready
    for(auto iter = hostPrivatePlayerList->begin(); iter != hostPrivatePlayerList->end(); ++iter){
        if((*iter)->getIPAddress() == ip)
            (*iter)->setPlayerReady();
    }
    //send updated list to all clients
    sendUpdatedPlayerList();
}

void gameAdapter::slt_kickPlayer(QString ip)
{
    if (m_isHost){
        qDebug() << "[GA] kickPlayer received for ip " << ip;

        server->kickPlayer(ip);

        removeFromPlayerList(ip);
    }
}

void gameAdapter::slt_receivedPlayerLeft(QString ip)
{
    qDebug() << "[GA] receivedPlayerLeft for ip " << ip;

    //inform gameloop
    emit sig_announceGPPlayerLeftGame(ip);

    //remove the player from the local player list and send it to the clients
    slt_kickPlayer(ip);
}

void gameAdapter::slt_clientDisconnectedReceived(QString ip)
{
    qDebug() << "[GA HOST] receivedClientDisconnected for ip " << ip;
    if (!m_gameStarted){
        qDebug() << "[GA HOST] remove player from not started game";
        removeFromPlayerList(ip);
    }
    else{
        qDebug() << "[GA HOST] remove player from started game";
        sig_informGameplayPlayerLeftGame(ip);
        sendUpdatedPlayerList();
    }
}





void gameAdapter::slt_receivedNewPlayerlist(std::vector<publicPlayer*>* newplayerList)
{
    qDebug() << "[GA] NEW PLAYERLIST received";


    //if there is a local list then do the old - new comparison
    if (localPublicPlayerList->size() > 0){

        bool oldPlayer = false;

        qDebug() << "[GA] Check for Old Player";

        // 1. - find player NOT in the NEW list


        for (auto oldListIter = localPublicPlayerList->begin(); oldListIter != localPublicPlayerList->end(); ++oldListIter){
            qDebug() << "[GA] check for entry : " << (*oldListIter)->getPlayerName() << " - " << (*oldListIter)->getIPAddress();

                oldPlayer = false;

                qDebug() << "compare with ";
                for (auto newListIter = newplayerList->begin(); newListIter != newplayerList->end(); ++newListIter){
                    qDebug() << "[GA] " << (*newListIter)->getPlayerName() << " - " << (*newListIter)->getIPAddress();

                    if ((*newListIter)->getPlayerName() == (*oldListIter)->getPlayerName() && (*newListIter)->getIPAddress() == (*oldListIter)->getIPAddress()){
                        qDebug() << "Old player in still in new list";
                        oldPlayer = true;
                        break;
                    }
                }
                if (!oldPlayer){
                    // not in NEW list anymore -> signal GUI
                    emit sig_playerLeftLobby((*oldListIter)->getPlayerName(), (*oldListIter)->getIPAddress());
                }
            }
        }

    // 2. - compare who is new ?

    bool newPlayer = true;


    qDebug() << "[GA] check for new player";
    for (auto newListIter = newplayerList->begin(); newListIter != newplayerList->end(); ++newListIter){
        qDebug() << "[GA] entry : " << (*newListIter)->getPlayerName() << " - " << (*newListIter)->getIPAddress();

        //compare new player with all old players
        if (localPublicPlayerList->size() > 0){
            qDebug() << "compare with ";
            for (auto oldListIter = localPublicPlayerList->begin(); oldListIter != localPublicPlayerList->end(); ++oldListIter){

                qDebug() << "[GA] " << (*oldListIter)->getPlayerName() << " - " << (*oldListIter)->getIPAddress();

                //if player allready on list, break loop to compare next
                //if(*newListIter == *oldListIter){
                if ((*newListIter)->getPlayerName() == (*oldListIter)->getPlayerName() && (*newListIter)->getIPAddress() == (*oldListIter)->getIPAddress()){
                    qDebug() << "[GA] NEW PLAYERLIST - player already on list";
                    newPlayer = false;
                    break;
                }
            }
        }
        //if player was not in old list (newPlayer still set) -> signal GUI
        if (newPlayer){
            qDebug() << "[GA] NEW PLAYERLIST - NEW PLAYER IN LIST " << (*newListIter)->getPlayerName() << " WITH IP: " << (*newListIter)->getIPAddress();
            emit sig_newPlayerInLobby((*newListIter)->getPlayerName(),(*newListIter)->getIPAddress());
        }
        newPlayer = true;
    }

    //assign the playerlist to local
    assignNewLocalPublicPlayerList(newplayerList);

    qDebug() << "new list localPlayer " << localPublicPlayerList << "listsize = " << localPublicPlayerList->size();
}

void gameAdapter::slt_clientConnected()
{
    m_clientConnected = true;
    client->addSelf(localPlayer->getPlayerName());


    //connections from here:




    //if connected go to lobby - network answers with allready attending players ? Maybe param to lobby ?
    //if (!isHost)
//        lobby();
}

void gameAdapter::slt_clientDisconnected()
{
    m_clientConnected = false;

    qDebug() << "[GA]: Client connection close (kicked?)";


    if (localPublicPlayerList != NULL){
        //clear localPublicPlayerList
        qDebug() << "clear local list with size " << localPublicPlayerList->size();
        if (localPublicPlayerList->size() > 0){
            for(auto iter = localPublicPlayerList->begin(); iter != localPublicPlayerList->end();){

                //inform GUI
                emit sig_playerLeftLobby((*iter)->getPlayerName(), (*iter)->getIPAddress());

                qDebug() << "remove " << (*iter)->getPlayerName() << " " << (*iter)->getIPAddress();
                iter = localPublicPlayerList->erase(iter);
            }
        }
    }

    emit sig_gameEnded();
}

void gameAdapter::slt_playedCardsReceived(QString ip, std::vector<AbstractCard::CardTypes> cards)
{
    qDebug() << "[GA]: played cards received";

    if (cards.size() > 0){
        for(auto iter = localPublicPlayerList->begin(); iter != localPublicPlayerList->end(); ++iter){
            if ((*iter)->getIPAddress() == ip){

                //emit signal to the GUI saying which cards where played by the player
                emit sig_playedCards((*iter)->getPlayerName(), gameAdapter::vectorToQVariant(cards));

                //fill the qproperty to show the last played card on the screen

                m_cardPlayed = cards.at(cards.size()-1);
                emit qmlPlayedCardChanged();
                //inform GUI for handcards too


                //and finally an additional signals to emit for GUI change and animation
                for(auto iter = cards.begin(); iter != cards.end(); ++iter){

                    //play sounds
                    if (*iter == AbstractCard::Nope)
                        slt_playSound(Audio::AnnouncerNope);
                    if (*iter == AbstractCard::Defuse)
                        slt_playSound(Audio::AnnouncerDefuse);
                    if (*iter == AbstractCard::Attack)
                        slt_playSound(Audio::AnnouncerAttack);
                    if (*iter == AbstractCard::Skip)
                        slt_playSound(Audio::AnnouncerSkip);
                }
                break;
            }
        }
    }
    else
        qDebug() << "[GA] Error on playedCardsReceived";
}

void gameAdapter::slt_cardsReceivedForPlayerHandCards(std::vector<AbstractCard::CardTypes> cards)
{
    qDebug() << "[GA]: cards for player hand received";

    for(auto iter = cards.begin(); iter != cards.end(); ++iter){

        //add cards to player hand
        localPlayer->playerDeck->addCard(*iter);

        //replace last card received
        m_drawCard = *iter;
        emit qmlDrawnCardChanged();

        //if expl kitten -> signal for animation
        if ((*iter) == AbstractCard::ExplosiveKitten){
            qDebug() << "Exploding kitten @ player";
            emit sig_explodingKittenDrawn();
        }

        //refresh GUI
        refreshGUIHandCards();
    }
}

void gameAdapter::slt_networkReceivedActivePlayer(QString ip)
{
    qDebug() << "[GA]: new active player received";
    qDebug() << "[GA] compare " << ip << " with " << localPlayer->getIPAddress();
    if (ip == localPlayer->getIPAddress()){
        qDebug() << "[GA] localplayer is ACTIVE";
        m_localPlayerTurn = true;
//        emit sig_localPlayersTurnBegun();
    }
    else
        if (m_localPlayerTurn){
            qDebug() << "[GA] localplayer is NOT ACTIVE";
            m_localPlayerTurn = false;
//            emit sig_localPlayersTurnEnded();
        }

    emit newActivePlayer(ip);
}

void gameAdapter::slt_networkChangedState(BaseState::playStates state)
{
    m_guiActive = false;
    activeState = state;
    //needs to be the first check that noping is true or false and DOES NOT interfere with later checks when showing gui in noping or normal mode
    if (activeState == BaseState::playStates::gatherNopeState){
        qDebug() << "[GA] - nope phase started, ACTIVATE !";
        m_guiActive = true;
        m_noping = true;
        emit sig_localPlayersTurnBegun();
    }
    else
        m_noping = false;

    qDebug() << "[GA] network state change received [" << state << "]";
    if (activeState == BaseState::playStates::playCardState){
        qDebug() << "active ? " << m_localPlayerTurn;
        if (m_localPlayerTurn){
            qDebug() << "[GA] - playCardstate, activating player !";
            emit sig_localPlayersTurnBegun();
            m_guiActive = true;
            m_endedPlayCardState = false;
        }
    }

    if (activeState == BaseState::playStates::activateCardState){
        qDebug() << "[GA] - nope phase ended, DEACTIVATE !";
        emit sig_localPlayersTurnEnded();
    }

    //then there was a card drawn
    if (activeState == BaseState::playStates::drawCardState)
        --m_amountOfCardsInDrawPile;

    //if player is in defuse state, he should defuse
    if (activeState == BaseState::playStates::defuseState){
        if (m_localPlayerTurn)
            qDebug() << "Player should defuse now";
    }

    if (activeState == BaseState::playStates::placeKittenState){
        if (m_localPlayerTurn)
            emit sig_placeKitten();
    }
}

void gameAdapter::slt_seeTheFutureCardsReceived(std::vector<AbstractCard::CardTypes> cards)
{
    qDebug() << "[GA]: see the future cards received";

    //if this is players turn, last card was a see the future, this signal is used to send the 3 "visible" cards
//    if ((m_cardPlayed == AbstractCard::SeeTheFuture) && (m_localPlayerTurn))
    if (m_localPlayerTurn){
        qDebug() << "[GA]: showing see future cards";
        emit sig_seeTheFuture(vectorToQVariant(cards));
    }
}



void gameAdapter::slt_shuffleReceived()
{
    qDebug() << "[GA]: suffle cards received";

    emit sig_shuffle();
}

void gameAdapter::slt_receivedNewCoPlayerCardAmount(QString ip, uint amount)
{
    emit sig_coplayersCardsOnHand(ip, QString::number(amount));
}

void gameAdapter::slt_receivedNewAmountForDrawPile(unsigned int newAmount)
{
    qDebug() << "[GA] received new amount in drawpile";
    setAmountOfCardsInDrawPile(newAmount);
}

void gameAdapter::slt_receivedPlayerStats(QMap<int, unsigned int> playerStatistics, QString playerList)
{
    qDebug() << "[TODO] wonLost";
    int cardsPlayed = playerStatistics.value(Logging::amountCards);
    int wonLost = 0;
    int defused = playerStatistics.value(Logging::amountDefuse);
    int noped = playerStatistics.value(Logging::amountNope);
    int special = playerStatistics.value(Logging::amountSpecialMoves);
    dbHelper->updateStats(cardsPlayed, wonLost, defused, noped, special, playerList);
    emit sig_dbHelperInit();
    emit sig_errorMessage("Stats updated!");
}

void gameAdapter::slt_receivedGameStats(QMap<int, QMap<QString, unsigned int> > gameBestOfStatistics)
{
    QString mostCardsPlayer = gameBestOfStatistics.value(Logging::amountCards).keys().first();
    unsigned int mostCards = gameBestOfStatistics.value(Logging::amountCards).value(mostCardsPlayer);
    QString mostDefusesPlayer = gameBestOfStatistics.value(Logging::amountDefuse).keys().first();
    unsigned int mostDefuses = gameBestOfStatistics.value(Logging::amountDefuse).value(mostDefusesPlayer);
    QString mostNopesPlayer = gameBestOfStatistics.value(Logging::amountNope).keys().first();
    unsigned int mostNopes = gameBestOfStatistics.value(Logging::amountNope).value(mostNopesPlayer);
    QString mostSpecialMovesPlayer = gameBestOfStatistics.value(Logging::amountSpecialMoves).keys().first();
    unsigned int mostSpecialMoves = gameBestOfStatistics.value(Logging::amountSpecialMoves).value(mostSpecialMovesPlayer);

    QVariantList gameStats;
    gameStats.push_back(mostCardsPlayer);
    gameStats.push_back(mostCards);
    gameStats.push_back(mostDefusesPlayer);
    gameStats.push_back(mostDefuses);
    gameStats.push_back(mostNopesPlayer);
    gameStats.push_back(mostNopes);
    gameStats.push_back(mostSpecialMovesPlayer);
    gameStats.push_back(mostSpecialMoves);

    QVariant gameStatsList(gameStats);

    emit gameStatsReceived(gameStatsList);
}


void gameAdapter::slt_networkReceivedGameStarting()
{
    qDebug() << "[GA] received startgame from client starting joined game...";
    startJoinedGame();

    //set matchIsRunning for GUI
    m_matchIsRunning = true;
}

void gameAdapter::slt_networkReceivedGameEnded()
{
    qDebug() << "[GA]: game ended received";

    m_matchIsRunning = false;

    emit sig_gameEnded();
}

void gameAdapter::slt_clientReceivedIP(QString ip)
{
    qDebug() << "[GA] client received public ip " << ip;
    localPlayer->setIPAddress(ip);
}

/*
 * adds card to the card buffer from playerHand to be played
 */
void gameAdapter::slt_addCardToBePlayed(int cardEnum)
{
    //check if card is in hand deck and add it to the buffer

//    AbstractCard::CardTypes card = (AbstractCard::CardTypes) cardEnum;
    AbstractCard::CardTypes card = static_cast<AbstractCard::CardTypes>(cardEnum);
    qDebug() << card << "##############";

    //buffer empty than random card can be added, allready one in, only the same can be added
    if (cardsToBePlayedBuffer.size() == 0 || (cardsToBePlayedBuffer.at(0) == card && cardsToBePlayedBuffer.size() < MAXCARDSTOBEPLAYEDAMOUNT)){
        if(card == localPlayer->playerDeck->getSpecificCard(card)){
            cardsToBePlayedBuffer.push_back(card);
            refreshGUIHandCards();
        }
    }
    else{
        //put card back to players hand needed ?
        qDebug() << "[GA] card not fitting ! or buffer full (max 3 cards)";
    }
}

/*
 * checks the buffer for the card and removes it and puts it back in playerHand
 */
void gameAdapter::slt_removeCardToBePlayed(int cardEnum)
{
//        AbstractCard::CardTypes card = (AbstractCard::CardTypes) cardEnum;
    AbstractCard::CardTypes card = static_cast<AbstractCard::CardTypes>(cardEnum);

//    if (cardsToBePlayedBuffer != NULL)  {
        for(auto iter = cardsToBePlayedBuffer.begin(); iter != cardsToBePlayedBuffer.end();){
            if ((*iter) == card){
                localPlayer->playerDeck->addCard(*iter);
                cardsToBePlayedBuffer.erase(iter);
                refreshGUIHandCards();
            }
            else
                 ++iter;
        }
        //    }
}

void gameAdapter::slt_clearCardBuffer()
{
    qDebug() << "[GA] - clear buffer, put cards back to playerhand";
    for(auto iter = cardsToBePlayedBuffer.begin(); iter != cardsToBePlayedBuffer.end();){
        localPlayer->playerDeck->addCard(*iter);
        iter = cardsToBePlayedBuffer.erase(iter);
    }
    refreshGUIHandCards();
}

void gameAdapter::slt_progressChanged(double percent)
{
    if (m_guiActive)
        emit sig_progressChanged(percent);
    else
        emit sig_progressChanged(0.0);

    //check for running low on time to deactivate player
    if (percent < 0.15){
        emit sig_localPlayersTurnEnded();
        slt_clearCardBuffer();
    }
}

void gameAdapter::slt_errorMessage(QString message)
{
    qDebug() << "[GA]: error received";

    emit sig_errorMessage(message);
}

void gameAdapter::slt_playerLost(QString ip)
{
    if(ip == localPlayer->getIPAddress()) {
        slt_playSound(Audio::AnnouncerLost);
    }
}

void gameAdapter::slt_playerWon(QString ip)
{
    if(ip == localPlayer->getIPAddress()) {
        slt_playSound(Audio::AnnouncerWon);
    }
}

QVariant gameAdapter::vectorToQVariant(std::vector<AbstractCard::CardTypes> cards)
{
    QVariantList cardList;

    for(size_t i = 0; i < cards.size(); i++) {
        cardList.push_back(cards[i]);
    }

    QVariant variantCards(cardList);
    return variantCards;
}

void gameAdapter::addKittenIntoDrawPileAtPosition(QString position)
{
    int pos = position.toInt();
    if((pos >= 0) && (pos <= m_amountOfCardsInDrawPile+1))
        emit sig_sendServerKittenPositionInDrawPile(pos);
    else
        emit sig_placeKitten();
}

QVariant gameAdapter::getSettings()
{
    std::vector<int> settings = dbHelper->getSettings();
    QString name = dbHelper->getActiveProfile();
    int lang = settings.at(0);
    int volFX = settings.at(1);
    int volMUSIC = settings.at(2);
    audio->setVolume(volFX, volMUSIC);
    selectLanguage(lang);
    QVariantList varSettings;

    for(size_t i = 0; i < settings.size(); i++) {
        varSettings.push_back(settings[i]);
    }
    varSettings.push_back(name);

    QVariant settingsList(varSettings);

    return settingsList;
}

void gameAdapter::saveSettings(int lang, int volFX, int volMUSIC, int fullscreen, QString name)
{
    dbHelper->setSettings(lang, volFX, volMUSIC, fullscreen);
    dbHelper->changeName(name);
    localPlayer->setPlayerName(name);
    selectLanguage(lang);
    audio->setVolume(volFX, volMUSIC);
}

void gameAdapter::slt_playLoop(int enm)
{
    audio->playLoop(enm);
}

void gameAdapter::slt_playSound(int enm)
{
    audio->playSound(enm);
}

void gameAdapter::slt_stopSound()
{
    audio->stopSound();
}

//ingame
