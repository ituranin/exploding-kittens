#ifndef GAMERULESTEST_H
#define GAMERULESTEST_H
#include <QTest>
#include "gameadapter.h"
#include "gameplaysettings.h"
#include "gameplay.h"

/**
 * @brief The gameRulesTest class tests the statemachine and playstates when gamelogic and rules apply
 */
class gameRulesTest : public QObject
{
    Q_OBJECT

//private slots:
private Q_SLOTS:


//    void initTestCase(){
//        qDebug("Starting Decktests");
//    }


//    void myEmptyTest(){
//        QVERIFY(1 != 2);
//    }

    /**
     * @brief testDrawRandomCardFromRandomPersionwith2Cards test player1 haveing played 2 of the same cards to get a random card from player 2. In the end there should be the card of player 2 in the deck of player 1 and player two has no card left
     */
    void testDrawRandomCardFromRandomPersionwith2Cards(){

        privatePlayer* player1 = new privatePlayer("Player1", "ipPlayer1");
        privatePlayer* player2 = new privatePlayer("Player2", "ipPlayer2");

        PlayerDeck* player1Deck = new PlayerDeck();
        PlayerDeck* player2Deck = new PlayerDeck();

        //cards are not in the deck anymore at this point
//        player1Deck.addCard(AbstractCard::BikiniCat);
//        player1Deck.addCard(AbstractCard::BikiniCat);

        //but in the playedCards buffer


        player2Deck->addCard(AbstractCard::Attack);

        player1->assignDeck(player1Deck);
        player2->assignDeck(player2Deck);

        std::vector<privatePlayer*>* attendingPlayer= new std::vector<privatePlayer*>();;
        attendingPlayer->push_back(player1);
        attendingPlayer->push_back(player2);

        qDebug() << "player amount is = " << attendingPlayer->size();

        Deck* m_discardPile = new Deck(Deck::emptyDeck,0);
        std::vector<PlayedCard*>* m_playedCards = new std::vector<PlayedCard*>;
        DrawDeck* m_drawPile = new DrawDeck(attendingPlayer->size());

//        for (int i = 0; i < attendingPlayer->size(); ++i) {
//            attendingPlayer->at(i)->assignDeck(new PlayerDeck(m_drawPile));
//        }


        //add played cards to buffer
        m_playedCards->push_back(new PlayedCard(AbstractCard::BikiniCat, "player1"));
        m_playedCards->push_back(new PlayedCard(AbstractCard::BikiniCat, "player1"));

        //create gameRule state
        ActivateCardState* activateCardState = new ActivateCardState(NULL, NULL,StateMachine::activateCardState, ACTIVATECARDSTATEDURATION, attendingPlayer, m_drawPile, m_playedCards, m_discardPile);

        activateCardState->setActivePlayer(0);  //set player1 active

        activateCardState->drawRandomCard();


        //QCOMPARE(player1Deck->getAmountOfCards(), AbstractCard::ExplosiveKitten);
        QVERIFY((player1Deck->countCards() == 1) && (player1Deck->getAmountOfCards(AbstractCard::Attack) == 1) && (player2Deck->countCards() == 0) && (player2Deck->getAmountOfCards(AbstractCard::Attack) == 0));


        //cleanup
        delete activateCardState;
        delete attendingPlayer;

        delete m_discardPile;
        delete m_playedCards;
        delete m_drawPile;

        //maybe inexistent allready...

        delete player1Deck;
        delete player2Deck;

        delete player1;
        delete player2;
    }

    /**
     * @brief testDrawRandomCardFromRandomPersionwith3Cards test player1 haveing played 3 same cards to get a random card from player 2. In the end there should be the card of player 2 in the deck of player 1 and player two has no card left
     */
    void testDrawRandomCardFromRandomPersionwith3Cards(){

        privatePlayer* player1 = new privatePlayer("Player1", "ipPlayer1");
        privatePlayer* player2 = new privatePlayer("Player2", "ipPlayer2");

        PlayerDeck* player1Deck = new PlayerDeck();
        PlayerDeck* player2Deck = new PlayerDeck();

        //cards are not in the deck anymore at this point
//        player1Deck.addCard(AbstractCard::BikiniCat);
//        player1Deck.addCard(AbstractCard::BikiniCat);

        //but in the playedCards buffer


        player2Deck->addCard(AbstractCard::Attack);

        player1->assignDeck(player1Deck);
        player2->assignDeck(player2Deck);

        std::vector<privatePlayer*>* attendingPlayer= new std::vector<privatePlayer*>();;
        attendingPlayer->push_back(player1);
        attendingPlayer->push_back(player2);

        qDebug() << "player amount is = " << attendingPlayer->size();

        Deck* m_discardPile = new Deck(Deck::emptyDeck,0);
        std::vector<PlayedCard*>* m_playedCards = new std::vector<PlayedCard*>;
        DrawDeck* m_drawPile = new DrawDeck(attendingPlayer->size());

//        for (int i = 0; i < attendingPlayer->size(); ++i) {
//            attendingPlayer->at(i)->assignDeck(new PlayerDeck(m_drawPile));
//        }


        //add played cards to buffer
        m_playedCards->push_back(new PlayedCard(AbstractCard::BikiniCat, "player1"));
        m_playedCards->push_back(new PlayedCard(AbstractCard::BikiniCat, "player1"));
        m_playedCards->push_back(new PlayedCard(AbstractCard::BikiniCat, "player1"));

        //create gameRule state
        ActivateCardState* activateCardState = new ActivateCardState(NULL, NULL,StateMachine::activateCardState, ACTIVATECARDSTATEDURATION, attendingPlayer, m_drawPile, m_playedCards, m_discardPile);

        activateCardState->setActivePlayer(0);  //set player1 active

        activateCardState->drawRandomCard();


        //QCOMPARE(player1Deck->getAmountOfCards(), AbstractCard::ExplosiveKitten);
        QVERIFY((player1Deck->countCards() == 1) && (player1Deck->getAmountOfCards(AbstractCard::Attack) == 1) && (player2Deck->countCards() == 0) && (player2Deck->getAmountOfCards(AbstractCard::Attack) == 0));


        //cleanup
        delete activateCardState;
        delete attendingPlayer;

        delete m_discardPile;
        delete m_playedCards;
        delete m_drawPile;

        //maybe inexistent allready...

        delete player1Deck;
        delete player2Deck;

        delete player1;
        delete player2;
    }

    /**
     * @brief testDrawRandomCardFromRandomPersionwith1FavorCard test player1 haveing played the favor card to get a random card from player 2. In the end there should be the card of player 2 in the deck of player 1 and player two has no card left
     */
    void testDrawRandomCardFromRandomPersionwith1FavorCard(){

        privatePlayer* player1 = new privatePlayer("Player1", "ipPlayer1");
        privatePlayer* player2 = new privatePlayer("Player2", "ipPlayer2");

        PlayerDeck* player1Deck = new PlayerDeck();
        PlayerDeck* player2Deck = new PlayerDeck();

        //cards are not in the deck anymore at this point
//        player1Deck.addCard(AbstractCard::BikiniCat);
//        player1Deck.addCard(AbstractCard::BikiniCat);

        //but in the playedCards buffer


        player2Deck->addCard(AbstractCard::Attack);

        player1->assignDeck(player1Deck);
        player2->assignDeck(player2Deck);

        std::vector<privatePlayer*>* attendingPlayer= new std::vector<privatePlayer*>();;
        attendingPlayer->push_back(player1);
        attendingPlayer->push_back(player2);

        qDebug() << "player amount is = " << attendingPlayer->size();

        Deck* m_discardPile = new Deck(Deck::emptyDeck,0);
        std::vector<PlayedCard*>* m_playedCards = new std::vector<PlayedCard*>;
        DrawDeck* m_drawPile = new DrawDeck(attendingPlayer->size());

//        for (int i = 0; i < attendingPlayer->size(); ++i) {
//            attendingPlayer->at(i)->assignDeck(new PlayerDeck(m_drawPile));
//        }


        //add played cards to buffer
        m_playedCards->push_back(new PlayedCard(AbstractCard::Favor, "player1"));

        //create gameRule state
        ActivateCardState* activateCardState = new ActivateCardState(NULL, NULL,StateMachine::activateCardState, ACTIVATECARDSTATEDURATION, attendingPlayer, m_drawPile, m_playedCards, m_discardPile);

        activateCardState->setActivePlayer(0);  //set player1 active

        activateCardState->drawRandomCard();


        //QCOMPARE(player1Deck->getAmountOfCards(), AbstractCard::ExplosiveKitten);
        QVERIFY((player1Deck->countCards() == 1) && (player1Deck->getAmountOfCards(AbstractCard::Attack) == 1) && (player2Deck->countCards() == 0) && (player2Deck->getAmountOfCards(AbstractCard::Attack) == 0));


        //cleanup
        delete activateCardState;
        delete attendingPlayer;

        delete m_discardPile;
        delete m_playedCards;
        delete m_drawPile;

        //maybe inexistent allready...

        delete player1Deck;
        delete player2Deck;

        delete player1;
        delete player2;
    }

    /**
     * @brief testDrawRandomCardFromRandomPersionwith1FavorCard test player1 haveing played the favor card to get a random card from another player. These others should combined have one card less then bevore and player1 the card
     */
    void testDrawRandomCardFromRandomPersionwith1FavorCard3OtherPlayers(){

        privatePlayer* player1 = new privatePlayer("Player1", "ipPlayer1");
        privatePlayer* player2 = new privatePlayer("Player2", "ipPlayer2");
        privatePlayer* player3 = new privatePlayer("Player3", "ipPlayer3");

        PlayerDeck* player1Deck = new PlayerDeck();
        PlayerDeck* player2Deck = new PlayerDeck();
        PlayerDeck* player3Deck = new PlayerDeck();

        player2Deck->addCard(AbstractCard::Attack);
        player3Deck->addCard(AbstractCard::Attack);

        player1->assignDeck(player1Deck);
        player2->assignDeck(player2Deck);
        player3->assignDeck(player3Deck);

        std::vector<privatePlayer*>* attendingPlayer= new std::vector<privatePlayer*>();;
        attendingPlayer->push_back(player1);
        attendingPlayer->push_back(player2);
        attendingPlayer->push_back(player3);

        qDebug() << "player amount is = " << attendingPlayer->size();

        Deck* m_discardPile = new Deck(Deck::emptyDeck,0);
        std::vector<PlayedCard*>* m_playedCards = new std::vector<PlayedCard*>;
        DrawDeck* m_drawPile = new DrawDeck(attendingPlayer->size());

//        for (int i = 0; i < attendingPlayer->size(); ++i) {
//            attendingPlayer->at(i)->assignDeck(new PlayerDeck(m_drawPile));
//        }


        //add played cards to buffer
        m_playedCards->push_back(new PlayedCard(AbstractCard::Favor, "player1"));

        //create gameRule state
        ActivateCardState* activateCardState = new ActivateCardState(NULL, NULL,StateMachine::activateCardState, ACTIVATECARDSTATEDURATION, attendingPlayer, m_drawPile, m_playedCards, m_discardPile);

        activateCardState->setActivePlayer(0);  //set player1 active

        activateCardState->drawRandomCard();


        //QCOMPARE(player1Deck->getAmountOfCards(), AbstractCard::ExplosiveKitten);
        QVERIFY((player1Deck->countCards() == 1) && (player1Deck->getAmountOfCards(AbstractCard::Attack) == 1) && (((player2Deck->countCards() == 0) && (player3Deck->countCards() == 1)) || ((player2Deck->countCards() == 1) && (player3Deck->countCards() == 0))));


        //cleanup
        delete activateCardState;
        delete attendingPlayer;

        delete m_discardPile;
        delete m_playedCards;
        delete m_drawPile;

        //maybe inexistent allready...

        delete player1Deck;
        delete player2Deck;
        delete player3Deck;

        delete player1;
        delete player2;
        delete player3;
    }

    /**
     * @brief testEndgameStateLogFileCreation that the complete game bestof logfiles are calculatet propperly
     */
    void testEndgameStateLogFileCreation(){

        privatePlayer* player1 = new privatePlayer("Player1", "ipPlayer1");
        privatePlayer* player2 = new privatePlayer("Player2", "ipPlayer2");
        privatePlayer* player3 = new privatePlayer("Player3", "ipPlayer3");


        std::vector<privatePlayer*>* attendingPlayer= new std::vector<privatePlayer*>();;
        attendingPlayer->push_back(player1);
        attendingPlayer->push_back(player2);
        attendingPlayer->push_back(player3);

        qDebug() << "player amount is = " << attendingPlayer->size();

        Deck* m_discardPile = new Deck(Deck::emptyDeck,0);
        std::vector<PlayedCard*>* m_playedCards = new std::vector<PlayedCard*>;
        DrawDeck* m_drawPile = new DrawDeck(attendingPlayer->size());

//        for (int i = 0; i < attendingPlayer->size(); ++i) {
//            attendingPlayer->at(i)->assignDeck(new PlayerDeck(m_drawPile));
//        }

        //add some statistics to the players

        //player 1 has played 2 defuse and 2 in total
        player1->logPlayedCard(AbstractCard::Defuse);
        player1->logPlayedCard(AbstractCard::Defuse);

        //player 2 has played 1 defuse and 3 in total
        player2->logPlayedCard(AbstractCard::Defuse);
        player2->logPlayedCard(AbstractCard::Favor);
        player2->logPlayedCard(AbstractCard::ShyBladder);

        //player 3 has played 1 nope and 1 in total
        player3->logPlayedCard(AbstractCard::Nope);


        //statistics finished
        //player 1 - most defuses (2)
        //player 3 - most nopes (1)
        //player 2 - most cards (3)
        //no one - most special combos (0)


        //create gameRule state
        EndGameState* endGameState = new EndGameState(NULL, NULL,StateMachine::endGameState, ENDGAMESTATEDURATION, attendingPlayer, m_drawPile, m_playedCards, m_discardPile);

        //connect test signals
        endGameState->connectTestSignals();

        endGameState->gameLogic();

        //read gameStatistics
        qDebug() << "most defuses" << endGameState->gameBestOfStatistics[Logging::amountDefuse];
        qDebug() << "most nopes" << endGameState->gameBestOfStatistics[Logging::amountNope];
        qDebug() << "most cards" << endGameState->gameBestOfStatistics[Logging::amountCards];
        qDebug() << "most special combos" << endGameState->gameBestOfStatistics[Logging::amountSpecialMoves];

        qDebug() << endGameState->gameBestOfStatistics[Logging::amountSpecialMoves].keys();
        qDebug() << endGameState->gameBestOfStatistics[Logging::amountSpecialMoves].values();

        QMap<QString, uint> compareQMap;
        compareQMap.insert("Player1", 2);
//        QCOMPARE(endGameState->gameBestOfStatistics[Logging::amountDefuse].keys(), compareQMap.keys());
        //QCOMPARE(player1Deck->getAmountOfCards(), AbstractCard::ExplosiveKitten);
//        QVERIFY((player1Deck->countCards() == 1) && (player1Deck->getAmountOfCards(AbstractCard::Attack) == 1) && (((player2Deck->countCards() == 0) && (player3Deck->countCards() == 1)) || ((player2Deck->countCards() == 1) && (player3Deck->countCards() == 0))));


        //cleanup
        delete endGameState;
        delete attendingPlayer;

        delete m_discardPile;
        delete m_playedCards;
        delete m_drawPile;

        //maybe inexistent allready...

        delete player1;
        delete player2;
        delete player3;
    }

    /**
     * @brief testGameplayPlayerLeaves test when a client leaves the game
     */
    void testGameplayPlayerLeaves(){
        privatePlayer* player1 = new privatePlayer("Host Player", "::ffff:127.0.0.1");
        privatePlayer* player2 = new privatePlayer("Client Player1", "::ffff:127.0.0.2");
        privatePlayer* player3 = new privatePlayer("Client Player2", "::ffff:127.0.0.3");


        std::vector<privatePlayer*>* attendingPlayer= new std::vector<privatePlayer*>();;
        attendingPlayer->push_back(player1);
        attendingPlayer->push_back(player2);
        attendingPlayer->push_back(player3);

        Server* server = new Server();

        hostGameplay* hostgp = new hostGameplay(attendingPlayer);
        hostgp->assignServer(server);

        hostgp->start();

        hostgp->slt_playerLeftGame(player2->getIPAddress());

    }

    /**
     * @brief testGameplayPlayerLeaves2PlayerGame test when a client leaves a 2 player game so the game ends
     */
    void testGameplayPlayerLeaves2PlayerGame(){
        privatePlayer* player1 = new privatePlayer("Host Player", "::ffff:127.0.0.1");
        privatePlayer* player2 = new privatePlayer("Client Player1", "::ffff:127.0.0.2");


        std::vector<privatePlayer*>* attendingPlayer= new std::vector<privatePlayer*>();;
        attendingPlayer->push_back(player1);
        attendingPlayer->push_back(player2);

        Server* server = new Server();

        hostGameplay* hostgp = new hostGameplay(attendingPlayer);
        hostgp->assignServer(server);

        hostgp->start();

        hostgp->slt_playerLeftGame(player2->getIPAddress());

    }

    /**
     * @brief testGameplayPlayerLeavesGame test when a client who is the host leaves a 2 player game so the game ends
     */
    void testGameplayPlayerLeavesAsHostTheGame(){
        privatePlayer* player1 = new privatePlayer("Host Player", "::ffff:127.0.0.1");
        privatePlayer* player2 = new privatePlayer("Client Player1", "::ffff:127.0.0.2");


        std::vector<privatePlayer*>* attendingPlayer= new std::vector<privatePlayer*>();;
        attendingPlayer->push_back(player1);
        attendingPlayer->push_back(player2);

        Server* server = new Server();

        hostGameplay* hostgp = new hostGameplay(attendingPlayer);
        hostgp->assignServer(server);

        hostgp->start();

        hostgp->slt_playerLeftGame(player1->getIPAddress());

    }

//    void cleanupTestCase(){
//        qDebug("called after myFirstTest and mySecondTest");
//    }
};

#endif // GAMERULESTEST_H
