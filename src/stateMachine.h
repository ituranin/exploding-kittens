#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include <QTime>
#include <QTimer>
#include <QDebug>
#include <QObject>
#include <string>
#include <memory>
#include <QSignalMapper>
#include "player.h"
#include "cards.h"
#include "gameplaysettings.h"

/**
 * @brief The BaseState class is the base class all other classes derive from.
 */
class BaseState : public QObject //abstractState
{
    Q_OBJECT
public:

    /**
     * @brief The playStates enum holds the name of the different states
     */
    enum playStates{
        playCardState,
        gatherNopeState,
        activateCardState,
        drawCardState,
        defuseState,
        placeKittenState,
        playerLostState,
        endGameState,
        switchPlayerState,
        stateMachine
    };

    Q_ENUM(playStates)

protected:

    /**
     * @brief attendingPlayer is a pointer to the attenting player list for ingame rule appliment
     */
    std::vector<privatePlayer*>* attendingPlayer;       //deletion happens in gamePlay, care !

    /**
     * @brief nextState is the state which by default is switched to
     */
    BaseState* nextState;

    /**
     * @brief nextState_A is the Slot A for a next State
     */
    BaseState* nextState_A;

    /**
     * @brief nextState_B is the Slot B for a next State
     */
    BaseState* nextState_B;

    /**
     * @brief name is an alternate naming for the state
     */
    BaseState::playStates name;

    /**
     * @brief setStateToSwitchState is set to true when rules apply and no more waiting for a player signal is needed. Is set to false on state switch
     */
    bool setStateToSwitchState = false;

    /**
     * @brief duration is the time bevor a state switch is initiated
     */
    int duration = 0;

    /**
     * @brief currentTime holds the current time to compare the duration is passed
     */
    int currentTime = 0;

    /**
     * @brief amountOfPlayedCards is for better calculation when cards from players arrive
     */
    static int amountOfPlayedCards;

    /**
     * @brief stateActive is true when the actual state is active
     */
    bool stateActive = false;

    /**
     * @brief m_gameResumed is used in the event a player leaves a game and the needed roll back is finished
     */
    bool m_gameResumed = false;

    /**
     * @brief m_positionOfLeaver holds the index in the attenting player list of the player who left
     */
    unsigned int m_positionOfLeaver = 0;

    /**
     * @brief drawPile is a reference to the drawPile where cards are taken in DrawCard state
     */
    DrawDeck* drawPile;

    /**
     * @brief discardPile is a referemce to the discardPile where cards are put at the end of a turn where cards are played
     */
    Deck* discardPile;

    /**
     * @brief playedCards is a buffer when cards where played including their information who they played. Its used in the event of a roll back to send the cards to their former owner
     */
    std::vector<PlayedCard*>* playedCards;

    /**
     * @brief skipped for game Rules (no card is drawn when true)
     */
    bool skipped = false;

    /**
     * @brief thisPlayerExtraRound for game rules. No player switch is applied, same player plays again
     */    
    static bool thisPlayerExtraRound;// = false;

    /**
     * @brief nextPlayerExtraRound for game rules. Next player plays 2 Rounds
     */
    static bool nextPlayerExtraRound;// = false;

    /**
     * @brief activePlayer holds the index of the player whos playing
     */
    static int activePlayer;

    /**
     * @brief gameLogic is parsed in every update run inside the state machine. Game Rules not inside a Slot are inside this
     */
    virtual void gameLogic() = 0;

    /**
     * @brief checkCards is a security routine to check if the transmitted cards are in the players deck on the server side
     * @return bool true or false if the check passed
     */
    virtual bool checkCards(std::vector<AbstractCard::CardTypes> &cards);

    /**
     * @brief stateCleanUp does work after the state finished and its about to be switched
     */
    virtual void stateCleanUp() = 0;

public:

    /**
     * @brief BaseState constructor
     * @param next_a standart state where to switch and the state in slot A
     * @param next_b additional state where applicable fro slot B
     * @param name of the State
     * @param duration of the state in seconds
     * @param playerlist holding all the players when a state needs access to them
     * @param drawPile where needed
     * @param playedCards where needed
     * @param discardPile where needed
     */
    BaseState(BaseState* next_a, BaseState *next_b, playStates name, int duration, std::vector<privatePlayer*>* playerlist, DrawDeck *drawPile, std::vector<PlayedCard*>* playedCards, Deck* discardPile);

    /**
     * @brief BaseState constructor in smaller form
     * @param next_a standart state where to switch and the state in slot A
     * @param next_b additional state where applicable fro slot B
     * @param name of the State
     * @param duration of the state in seconds
     */
    BaseState(BaseState* next_a, BaseState *next_b, playStates name, int duration);

    /**
     * @brief BaseState constructor needed for empty stateMachine state object
     */
    BaseState();

    /**
     * @brief update is called from the gamePlay
     * @param timeStamp is the actual time
     * @param refTime is the time since state switch
     * @return the next state when time is up or a hard stateSwitch occured
     */
    virtual BaseState* update(int &timeStamp, int refTime);

    /**
     * @brief setNextState sets the state which follows after this state
     * @param next state
     */
    virtual void setNextState(BaseState* next);

    /**
     * @brief setNextState_A sets the nextState in Slot A
     * @param next_A Slot A state
     */
    virtual void setNextState_A(BaseState* next_A);

    /**
     * @brief setNextState_B sets the nextState in Slot B
     * @param next_B Slot B state
     */
    virtual void setNextState_B(BaseState* next_B);

    /**
     * @brief getName returns the states name
     * @return name of the state
     */
    virtual playStates getName() const;

    /**
     * @brief getNextState returns the next state following after this state
     * @return name of the next state
     */
    virtual BaseState* getNextState() const;

    /**
     * @brief getNextState_A returns the state in Slot A
     * @return state Pointer
     */
    virtual BaseState* getNextState_A() const;

    /**
     * @brief getNextState_B returns the state in Slot B
     * @return state Pointer
     */
    virtual BaseState* getNextState_B() const;

    /**
     * @brief ~BaseState destructor
     */
    virtual ~BaseState();

    /**
     * @brief assignAttentingPlayer assigns a pointer to the player list in the actual game
     * @param players attending this game
     */
    void assignAttentingPlayer(std::vector<privatePlayer*>* players);

   /**
    * @brief setGameRestartedBecauseOfLeave is set from GP when a player left to set the switchState to resume on last player or if the player left to the next one
    * @param playerPosLeft of the leaver
    */
   void setGameRestartedBecauseOfLeave(unsigned int playerPosLeft);


signals:

   /**
    * @brief sig_changedState is emitted on every state transition
    * @param state which the game transitioned
    */
   void sig_changedState(BaseState::playStates state);   //connected to GP, Server

   /**
    * @brief sig_amountOfCardsChanged is emmited everytime a card is played in playCard or in the NopeState
    * @param ip the player whose cards changed
    * @param amount of cards he actual has
    */
   void sig_amountOfCardsChanged(QString ip, unsigned int amount); //connected to server

   /**
    * @brief sig_drawPileAmountChanged is emitted on every card action done on the drawPile
    * @param amount is the amount of cards in the drawPile
    */
   void sig_drawPileAmountChanged(unsigned int amount);

   /**
    * @brief sig_refreshTimer sends the percentage of remaining time in state
    * @param percentValue of the time remaining
    */
   void sig_refreshTimer(double percentValue);

private:

   /**
     * @brief signalMapper not needed so far
     */
   QSignalMapper *signalMapper;
};

//-> state Def from here

/**
 * @brief The PlayCardState class gathers the cards a player wants to play. If played the states follow the examination and game rules if not the player ends it turn
 */
class PlayCardState : public BaseState
{
    Q_OBJECT

public:
    using BaseState::BaseState;
    virtual void gameLogic();
    virtual void stateCleanUp();
    virtual ~PlayCardState();

signals:

    /**
     * @brief sig_playerPlayedCard is emitted when a player plays a card (to inform the other player)
     * @param ip of the playing player
     * @param cards who were played
     */
    void sig_playerPlayedCard(QString ip, std::vector<AbstractCard::CardTypes> cards);  //connected to Server


public slots:

    /**
     * @brief slt_playedCardsReceived examines the cards a player played that round
     * @param ip of the playing player
     * @param cards who where played
     */
    void slt_playedCardsReceived(QString ip, std::vector<AbstractCard::CardTypes> cards);   //connected from Server
};

/**
 * @brief The GatherNopeState class examines the noping phase if a card played bevor is getting active or not (including a nope itself)
 */
class GatherNopeState : public BaseState
{
    Q_OBJECT

public:
    using BaseState::BaseState;
    virtual void gameLogic();
    virtual void stateCleanUp();
    virtual ~GatherNopeState();

signals:

    /**
     * @brief sig_playerPlayedCard is emitted when a player plays a NOPE Card (to inform the other player)
     * @param ip of the playing player
     * @param cards who were played
     */
    void sig_playerPlayedCard(QString ip, std::vector<AbstractCard::CardTypes> cards);

public slots:

    /**
     * @brief slt_playedCardsReceived examines the cards a player played that round. If it is a Nope it gets considered in the Game. If a valid nope is received the timer is cut to 15secs for others to react on
     * @param ip of the playing player
     * @param cards who where played
     */
    void slt_playedCardsReceived(QString ip, std::vector<AbstractCard::CardTypes> cards);   //connected from Server

private:

    /**
     * @brief noped is switched between true and false to determine if noping happened or not and which state is next due this
     */
    bool noped = 0;

    /**
     * @brief nopingStarted is activated when the noping Shorttimer should be activated
     */
    bool nopingStarted = false;

};

/**
 * @brief The ActivateCardState class activates the cards which where played in the play state and not noped in the nope. itchecks the cards in the buffer and applys the gamerules according to them
 */
class ActivateCardState : public BaseState
{
    Q_OBJECT
public:
    using BaseState::BaseState;
    virtual void gameLogic();
    virtual void stateCleanUp();
    virtual ~ActivateCardState();

    /**
     * @brief drawRandomCard draws a random card from a random player in the game
     */
    void drawRandomCard();

    //testMethods

    /**
     * @brief setActivePlayer used in test cases to set the active player
     * @param player the number of the player in the attending player list
     */
    void setActivePlayer(int player);

signals:

    /**
     * @brief sig_sendSeeTheFutureCards is emitted when a seeTheFuture Card is activated. it sends the top 3 cards from the drawPile to the playing player
     * @param ip of the player who played the card, receiving the 3 to view
     * @param cards on the drawPile
     */
    void sig_sendSeeTheFutureCards(QString ip, std::vector<AbstractCard::CardTypes> cards); //connected to Server

    /**
     * @brief sig_playerDrawCard is emitted when a player receives a new card
     * @param ip the player who tehts the card
     * @param drawnCard the card the player gets
     */
    void sig_playerDrawCard(QString ip, std::vector<AbstractCard::CardTypes> drawnCard);    //connected to Server

    /**
     * @brief sig_playerLostCard is emitted when a player loses a card to another player
     * @param ip the player who lost one card from his hand
     * @param drawnCard the card who was drawn
     */
    void sig_playerLostCard(QString ip, std::vector<AbstractCard::CardTypes> drawnCard);    //connected to Server

    /**
     * @brief sig_shuffleDeck is emitted when a shuffle card goes is in effect
     */
    void sig_shuffleDeck();


};

/**
 * @brief The DrawCardState class sends a card to the active player
 */
class DrawCardState : public BaseState
{
    Q_OBJECT

public:
    using BaseState::BaseState;
    virtual void gameLogic();
    virtual void stateCleanUp();
    virtual ~DrawCardState();

signals:

    /**
     * @brief sig_playerDrawCard is emitted when a player receives a new card
     * @param ip the player who tehts the card
     * @param drawnCard the card the player gets
     */
    void sig_playerDrawCard(QString ip, std::vector<AbstractCard::CardTypes> drawnCard);    //connected to Server

    /**
     * @brief sig_playerPlayedCard is emitted when a player draws a KITTEN Card (to inform the other player)
     * @param ip of the playing player
     * @param cards who were played
     */
    void sig_playerPlayedCard(QString ip, std::vector<AbstractCard::CardTypes> cards);
};

/**
 * @brief The DefuseCardState class checks for the time a player draws a kitten. He needs to send a defuse in this state to go on or the state switches to the losing branch
 */
class DefuseCardState : public BaseState
{
    Q_OBJECT

public:
    using BaseState::BaseState;
    virtual void gameLogic();
    virtual void stateCleanUp();
    virtual ~DefuseCardState();

signals:

    /**
     * @brief sig_playerPlayedCard is emitted when a player plays a DEFUSE Card (to inform the other player)
     * @param ip of the playing player
     * @param cards who were played
     */
    void sig_playerPlayedCard(QString ip, std::vector<AbstractCard::CardTypes> cards);

public slots:

    /**
     * @brief slt_playedCardsReceived examines the cards a player played that round. If it is a DEFUSE the state changes to Place Kitten State despite the lost game state
     * @param ip of the playing player
     * @param cards who where played
     */
    void slt_playedCardsReceived(QString ip, std::vector<AbstractCard::CardTypes> cards);   //connected from Server
};

/**
 * @brief The PlaceKittenState class awaits the position a player wants to put the kitten after a successful defuse state.
 */
class PlaceKittenState : public BaseState
{
    Q_OBJECT

public:
    using BaseState::BaseState;
    virtual void gameLogic();
    virtual void stateCleanUp();
    virtual ~PlaceKittenState();

public slots:

    /**
     * @brief slt_placeKittenReceived puts the Kitten at the transmitted position in the drawpile
     * @param ip of the player
     * @param position in the drawPile
     */
    void slt_placeKittenReceived(QString ip, int position); //connected from Server
};

/**
 * @brief The PlayerLostState class does the deactivation and informing of others when a player loses the round
 */
class PlayerLostState : public BaseState
{
    Q_OBJECT

public:
    using BaseState::BaseState;
    virtual void gameLogic();
    virtual void stateCleanUp();
    virtual ~PlayerLostState();

signals:

    /**
     * @brief sig_playerLost is emitted when the playerLost state is reached. Telling the actual player it lost
     * @param ip of the loser
     */
    void sig_playerLost(QString ip);    //connected to Server
};

/**
 * @brief The EndGameState class manages the ending of the game when only one player is left. The statistics are sent here and the signals that the game ended are emitted from here so the clean up begins
 */
class EndGameState : public BaseState
{
    Q_OBJECT

public:
    using BaseState::BaseState;
    virtual void gameLogic();
    virtual void stateCleanUp();
    virtual ~EndGameState();

    //testing
    /**
     * @brief connectTestSignals helper method to connect the signals to the test class
     */
    void connectTestSignals();

    /**
     * @brief gameBestOfStatistics holds the bestof statistics in testclass
     */
    QMap<Logging::logVals, QMap<QString, unsigned int>> gameBestOfStatistics;
public slots:
    /**
     * @brief slt_getGameStatistics is called for testin purposes connected to the testClass
     * @param gameBestOfStatistics holds the statistics
     */
    void slt_getGameStatistics(QMap<Logging::logVals, QMap<QString, unsigned int>> gameBestOfStatistics);

    //end testing



signals:

    /**
     * @brief sig_announceWinner is called when the game is in the endGame State, anouncing the last not losing player
     * @param ip of the winner
     */
    void sig_announceWinner(QString ip);    //connected to Server

    /**
     * @brief sig_gameEnded is emitted on game End
     */
    void sig_gameEnded();   //connected to Server, GP

    /**
     * @brief sig_sendPlayerStatistics sends the logged statistics from the game to the player via network
     * @param ip of the player
     * @param playerStatistics the logs for the player
     * @param playerList concatenated list of the players for the gui
     */
    void sig_sendPlayerStatistics(QString ip, QMap<Logging::logVals, unsigned int> playerStatistics, QString playerList);


    /**
     * @brief sig_sendGameStatistics sends the bestOf from the game to every client as end Screen info
     * @param gameBestOfStatistics of the game
     */
    void sig_sendGameStatistics(QMap<Logging::logVals, QMap<QString, unsigned int>> gameBestOfStatistics);

};

/**
 * @brief The SwitchPlayerState class switches between the active (not lost) players and informs them
 */
class SwitchPlayerState : public BaseState
{
    Q_OBJECT

public:
    using BaseState::BaseState;
    virtual void gameLogic();
    virtual void stateCleanUp();
    virtual ~SwitchPlayerState();

private:
    bool playerSet = false;

signals:

    /**
     * @brief sigSendActivePlayer is emitted when the switchplayer state has determined whos turn is next
     * @param activePlayerIPAdress the ip of the player active this round
     */
    void sigSendActivePlayer(QString activePlayerIPAdress); //connected to server
};


//StateMachine Logic here

/**
 * @brief The StateMachine class is the empty class holding the actual state so only the possible communication which is set up to the statemachine and the compatible state is possible
 */
class StateMachine : public BaseState{

public:
    using BaseState::BaseState;
    virtual void gameLogic();
    virtual void stateCleanUp();
    virtual ~StateMachine();
};

#endif // STATEMACHINE_H
