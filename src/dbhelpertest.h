#ifndef DBHELPERTEST_H
#define DBHELPERTEST_H
#include <QtTest>
#include "dbhelper.h"

/**
 * @brief The dbhelperTest class tests the dbhelper methods
 */
class dbhelperTest : public QObject
{

    Q_OBJECT
    /**
     * @brief test the instance of DBHelper to test
     */
    DBHelper* test = DBHelper::instance();

private Q_SLOTS:

    /**
     * @brief testCreateTable tests the table creation
     */
    void testCreateTable() {
        test->dropTable();
        QCOMPARE(test->createTable(), true);
    }

    /**
     * @brief testCreateProfile tests the profile creation
     */
    void testCreateProfile() {
        QCOMPARE(test->createProfile("Dieter"), true);
    }

    /**
     * @brief testChangeName tests the posibility to change the name of the active profile
     */
    void testChangeName() {
        QCOMPARE(test->changeName("Hans"), true);
    }

    /**
     * @brief testGetActiveProfile tests the method to get the active profiles name
     */
    void testGetActiveProfile() {
        QCOMPARE(test->getActiveProfile(), QString("Hans"));
    }

    /**
     * @brief testUpdateStats tests the method to update statistics
     */
    void testUpdateStats() {
        QCOMPARE(test->updateStats(40, 1, 3, 5, 10, "Dieter, Hans"), true);
    }

    /**
     * @brief testActivePlayerStats tests if correct statistics are returned
     */
    void testActivePlayerStats() {
        QVariantList list;
        list.push_back("Hans");
        list.push_back(40);
        list.push_back(1);
        list.push_back(0);
        list.push_back(3);
        list.push_back(5);
        list.push_back(10);
        list.push_back(405);
        list.push_back("Dieter, Hans");
        list.push_back(1);
        list.push_back(1000);
        QCOMPARE(test->getActivePlayerStats(), list);
    }

    /**
     * @brief testSetSettings tests if settings are saved to the table
     */
    void testSetSettings() {
        QCOMPARE(test->setSettings(1, 80, 60, 0), true);
    }

    /**
     * @brief testGetSettings tests if the correct settings are returned
     */
    void testGetSettings() {
        std::vector<int> vec;
        vec.push_back(1);
        vec.push_back(80);
        vec.push_back(60);
        vec.push_back(0);
        QCOMPARE(test->getSettings(), vec);
        test->dropTable();
    }

};

#endif // DBHELPERTEST_H
