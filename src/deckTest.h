#ifndef DECKTEST_H
#define DECKTEST_H
#include <QtTest>
#include "deck.h"

/**
 * @brief The decktest class tests the deck
 */
class decktest : public QObject
{
    Q_OBJECT

//private slots:
private Q_SLOTS:


//    void initTestCase(){
//        qDebug("Starting Decktests");
//    }


    /**
     * @brief testCompleteDeck testing for complete standart deck, containing all 13 card types times its amounts (56)
     */
    void testCompleteDeck(){
        int zaehler = 0;
        DrawDeck testDeck(5);   //5 players so the deck contains all cards
        zaehler = testDeck.countCards();
        QCOMPARE(zaehler, 56);
    }

    /**
     * @brief testPlayDeckWith2Players testing for PlayDeck for 2 Players containing 56 Cards - 3 Kittens = 53
     */
    void testPlayDeckWith2Players(){
        int zaehler = 0;
        DrawDeck testDeck(2);
        zaehler = testDeck.countCards();
        QCOMPARE(zaehler, 53);
    }

    /**
     * @brief testPlayDeckWith3Players testing for PlayDeck for 3 Players containing 56 Cards - 2 Kittens = 54
     */
    void testPlayDeckWith3Players(){
        int zaehler = 0;
        DrawDeck testDeck(3);
        zaehler = testDeck.countCards();
        QCOMPARE(zaehler, 54);
    }

    /**
     * @brief testPlayDeckWith4Players testing for PlayDeck for 4 Players containing 56 Cards - 1 Kittens = 55
     */
    void testPlayDeckWith4Players(){
        int zaehler = 0;
        DrawDeck testDeck(4);
        zaehler = testDeck.countCards();
        QCOMPARE(zaehler, 55);
    }

    /**
     * @brief testPlayDeckWith5Players testing for PlayDeck for 5 Players containing 56 Cards
     */
    void testPlayDeckWith5Players(){
        int zaehler = 0;
        DrawDeck testDeck(5);
        zaehler = testDeck.countCards();
        QCOMPARE(zaehler, 56);
    }

    //testing for PlayerStartDeck
    /**
     * @brief testPlayerStartDeck contains 1 defuse and 4 random cards not including a defuse or explKitten
     */
    void testPlayerStartDeck(){
        int zaehler = 0;
        DrawDeck* testDeck = new DrawDeck(1);   //cards for 1 player which is not happening, but necessary for testing
        PlayerDeck handDeck(testDeck);

        zaehler = handDeck.countCards();
        QCOMPARE(zaehler, 5);
        delete testDeck;
    }

    /**
     * @brief testFinalizeDrawDeckOnePlayer testing for finalized drawDeck with one player
     */
    void testFinalizeDrawDeckOnePlayer(){
        int zaehler = 0;
        DrawDeck* testDeck = new DrawDeck(1);   //cards for 1 player which is not happening, but necessary for testing, means from 56 cards - 4 kittens, -5 cards -> 47
        PlayerDeck handDeck(testDeck);
        testDeck->finalizeDeck();

        zaehler = testDeck->countCards();
        QCOMPARE(zaehler, 47);
        delete testDeck;
    }

    /**
     * @brief testFinalizeDrawDeckTwoPlayer testing for finalized drawDeck with two player
     */
    void testFinalizeDrawDeckTwoPlayer(){
        int zaehler = 0;
        DrawDeck* testDeck = new DrawDeck(2);   //cards for 2 player which is not happening, but necessary for testing, means from 56 cards - 3 kittens, -10 cards -> 43
        PlayerDeck handDeck1(testDeck);
        PlayerDeck handDeck2(testDeck);

        testDeck->finalizeDeck();

        zaehler = testDeck->countCards();
        QCOMPARE(zaehler, 43);
        delete testDeck;
    }

    /**
     * @brief testFinalizeDrawDeckThreePlayer testing for finalized drawDeck with three player
     */
    void testFinalizeDrawDeckThreePlayer(){
        int zaehler = 0;
        DrawDeck* testDeck = new DrawDeck(3);   //cards for 3 player which is not happening, but necessary for testing, means from 56 cards - 2 kittens, -15 cards -> 39
        PlayerDeck handDeck1(testDeck);
        PlayerDeck handDeck2(testDeck);
        PlayerDeck handDeck3(testDeck);

        testDeck->finalizeDeck();

        zaehler = testDeck->countCards();
        QCOMPARE(zaehler, 39);
        delete testDeck;
    }

    /**
     * @brief testFinalizeDrawDeckFourPlayer testing for finalized drawDeck with four player
     */
    void testFinalizeDrawDeckFourPlayer(){
        int zaehler = 0;
        DrawDeck* testDeck = new DrawDeck(4);   //cards for 4 player which is not happening, but necessary for testing, means from 56 cards - 1 kittens, -20 cards -> 35
        PlayerDeck handDeck1(testDeck);
        PlayerDeck handDeck2(testDeck);
        PlayerDeck handDeck3(testDeck);
        PlayerDeck handDeck4(testDeck);

        testDeck->finalizeDeck();

        zaehler = testDeck->countCards();
        QCOMPARE(zaehler, 35);
        delete testDeck;
    }

    /**
     * @brief testFinalizeDrawDeckFivePlayer testing for finalized drawDeck with five player
     */
    void testFinalizeDrawDeckFivePlayer(){
        int zaehler = 0;
        DrawDeck* testDeck = new DrawDeck(5);   //cards for 5 player which is not happening, but necessary for testing, means from 56 cards - 0 kittens, -25 cards -> 31
        PlayerDeck handDeck1(testDeck);
        PlayerDeck handDeck2(testDeck);
        PlayerDeck handDeck3(testDeck);
        PlayerDeck handDeck4(testDeck);
        PlayerDeck handDeck5(testDeck);

        testDeck->finalizeDeck();

        zaehler = testDeck->countCards();
        QCOMPARE(zaehler, 31);
        delete testDeck;
    }

    /**
     * @brief testFinalizeDrawDeckRandomization testing for finalized drawDeck with five player
     */
    void testFinalizeDrawDeckRandomization(){
        DrawDeck* testDeck = new DrawDeck(5);   //cards for 5 player which is not happening, but necessary for testing, means from 56 cards - 0 kittens, -25 cards -> 31
        PlayerDeck handDeck1(testDeck);
        PlayerDeck handDeck2(testDeck);
        PlayerDeck handDeck3(testDeck);
        PlayerDeck handDeck4(testDeck);
        PlayerDeck handDeck5(testDeck);

        testDeck->finalizeDeck();

        AbstractCard::CardTypes a = testDeck->readFirstCard();
        qDebug() << "a" << a;
        while(testDeck->readFirstCard() == a)
            testDeck->shuffleDeck();
        AbstractCard::CardTypes b = testDeck->readFirstCard();
        qDebug() << "b" << b;

        //QCOMPARE(a, b);
        QVERIFY(a != b);
        delete testDeck;
    }

    /**
     * @brief testFinalizeDrawDeckDrawCard testing for finalized drawDeck with one player
     */
    void testFinalizeDrawDeckDrawCard(){

        DrawDeck* testDeck = new DrawDeck(1);   //cards for 1 player which is not happening, but necessary for testing, means from 56 cards - 4 kittens, -5 cards -> 47
        PlayerDeck handDeck(testDeck);
        testDeck->finalizeDeck();

        int zaehler1 = testDeck->countCards();
        AbstractCard::CardTypes bla = testDeck->drawCard();
        int zaehler2 = testDeck->countCards();

        //QCOMPARE(zaehler, 47);
        QVERIFY(zaehler1 != zaehler2);

        delete testDeck;
    }

    /**
     * @brief testFinalizeDrawDeckInsertKittenAtPosition testing for finalized drawDeck with one player
     */
    void testFinalizeDrawDeckInsertKittenAtPosition(){

        DrawDeck* testDeck = new DrawDeck(1);   //cards for 1 player which is not happening, but necessary for testing, means from 56 cards - 4 kittens, -5 cards -> 47
        PlayerDeck handDeck(testDeck);
        testDeck->finalizeDeck();

        int zaehler1 = testDeck->countCards();
        qDebug() << zaehler1;
        AbstractCard::CardTypes bla = AbstractCard::ExplosiveKitten;
        testDeck->addKittenAtPosition(bla,3);   //card gets added before that position
        int zaehler2 = testDeck->countCards();
        qDebug() << zaehler2;

        QCOMPARE(testDeck->getCardAtPosition(3), AbstractCard::ExplosiveKitten);
        //QVERIFY(zaehler1 != zaehler2);

        delete testDeck;
    }

//    void cleanupTestCase(){
//        qDebug("called after myFirstTest and mySecondTest");
//    }


};

//QTEST_MAIN(decktest)
//#include "deckTest.moc"


#endif // DECKTEST_H
