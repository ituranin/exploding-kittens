#ifndef DBHELPER_H
#define DBHELPER_H

#include "QFile"
#include "QtSql/QSqlDatabase"
#include "QtSql/QSqlQuery"
#include "string"
#include "QVariantList"
#include "iostream"
#include <QSqlRecord>
using namespace std;

/**
 * @brief SETTINGSCOUNT counts how many settings there are in the game which can be changed
 */
static const int SETTINGSCOUNT = 4;

/**
 * @brief The DBHelper class is used for database access, storing player statistics and storing settings
 * It can be extended to support multiple profiles. It is designed after the singleton pattern to prevent
 * multiple access to the same database.
 */
class DBHelper
{
public:

    /**
     * @brief changeName is used to change the name of the currently active profile
     * @param newname is the name that should be stored
     * @return true when the name was successfullly changed
     */
    bool changeName(QString newname);

    /**
     * @brief updateStats is used to update the statistics of the currently active profile
     * @param cp the amount of cards that were played
     * @param wl 1 if the player won and 0 if he lost
     * @param d the amount of defuses that were played
     * @param n the amount of nopes that were played
     * @param spec the amount of special moves
     * @param foes the name of the players the player played against
     * @return true if the statistics were successfully updated
     */
    bool updateStats(int cp, int wl, int d, int n, int spec, QString foes);

    /**
     * @brief getStats is used to retrieve the statistics that are stored in the database for the currently active player.
     * It returns an integer array. This practice should not be done anymore and will be taken in account for further projects.
     * Still it wasn't redone due to it being complete.
     * @return the statistics that are stored in the database for the current player
     */
    int* getStats();

    /**
     * @brief getActivePlayerStats is used to get the statistics of the currently active player from the database
     * This method is used to provide the GUI with qml readable data types
     * @return a list of all statistics of the active player
     */
    QVariantList getActivePlayerStats();

    /**
     * @brief getNames prints all names of all profiles in the database
     * It is useless in the current state of this class, but the dbhelper can be easily extended to support multiple profiles.
     */
    void getNames();

    /**
     * @brief createProfile is used to create a profile on the first start of the game
     * @param name is the name under which the statistics should be stored and is the playername shown to other players
     * @return true if the profile was successfully created
     */
    bool createProfile(QString name);

    /**
     * @brief createTable is used to create database tables for profiles and settings
     * @return true when the tables were successfully created
     */
    bool createTable();

    /**
     * @brief dropTable is used to delete all tables in the database and is intended for testing only
     */
    void dropTable();

    /**
     * @brief getActiveProfile is used to get the player name of the currently active profile
     * @return the playername of the active profile
     */
    QString getActiveProfile();

    /**
     * @brief getSettings is used to retrieve the settings from the database
     * @return a vector of the stored settings
     */
    std::vector<int> getSettings();

    /**
     * @brief setSettings is used to store the settings which were changed in the GUI
     * @param language 0 for english and 1 for german
     * @param volFX the volume of the fx sound and announcer
     * @param volMUSIC the volume of the background music
     * @param fullscreen 0 for windowed mode and 1 for fullscreen
     * @return true if the settings were successfully stored
     */
    bool setSettings(int language, int volFX, int volMUSIC, int fullscreen);

    /**
     * @brief instance is used to create and instance of this class and return it or just return it if
     * one allready exists
     * @return the DBHelper instance
     */
    static DBHelper* instance() {
        if(!h_instance) {
            h_instance = new DBHelper();
        }
        return h_instance;
    }
private:

    /**
     * @brief db the database which should be written to
     */
    QSqlDatabase db;

    /**
     * @brief playerExists is used to check if the player exists
     * @param name the name of the player that is looked for
     * @return true if the player exists
     */
    bool playerExists(QString name);

    /**
     * @brief h_instance is the instance of this class
     */
    static DBHelper *h_instance;

    /**
     * @brief DBHelper is the constructor of this class
     */
    DBHelper();
};

#endif // DBHELPER_H
