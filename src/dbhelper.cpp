#include "dbhelper.h"
#include "QDebug"

#define WIN_POINTS 10
#define LOSS_POINTS 1
#define DEFUSE_POINTS 10
#define NOPE_POINTS 5
#define SPECIAL_POINTS 30
#define FOES_POSITION 7

DBHelper *DBHelper::h_instance = 0;

DBHelper::DBHelper()
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("profile.db");

    // checks if save file is present
    // if not -> create it, create empty content
    if(QFile::exists("profile.db")) {
        qDebug() << "db exists!";
    }
    if(db.open()) {
        qDebug() << "db opened";
    } else {
        qDebug() << "db open failed!";
    }

    if(createTable()) {
        qDebug() << "table created";
    }

}

bool DBHelper::createProfile(QString name) {

    if(!playerExists(name)) {
        QSqlQuery query;
        query.prepare("UPDATE profile SET active = 0 WHERE active = 1");

        if(query.exec()) {
            qDebug() << "update active succeed";
            query.finish();
        } else {
            qDebug() << "update active failed!";
        }

        query.prepare("INSERT INTO profile(nickname, cardsPlayed, wins, losses, defused, nopes, special, exp, lfoes, active) VALUES ((:name), 0, 0, 0, 0, 0, 0, 0, 'none', 1)");
        query.bindValue(":name", name);

        if(query.exec()) {
            qDebug() << "profile created";
            query.finish();
            return true;
        } else {
            qDebug() << "profile created query failed!";
            return false;
        }
    } else {
        qDebug() << "profile not created";
        return false;
    }
}

bool DBHelper::createTable()
{
    QSqlQuery query;

    query.prepare("CREATE TABLE IF NOT EXISTS profile ( nickname VARCHAR(30), cardsPlayed INTEGER,"
                  " wins INTEGER, losses INTEGER, defused INTEGER, nopes INTEGER, special INTEGER,"
                  " exp INTEGER, lfoes TEXT, active INTEGER )");

    if(!query.exec()){
        qDebug() << "query failed! profile table creation failed";
        return false;
    } else {
        qDebug() << "query successful! profile table created";
        query.finish();
    }

    query.prepare("CREATE TABLE IF NOT EXISTS settings (language INTEGER, volFX INTEGER, volMUSIC INTEGER, fullscreen INTEGER)");

    if(!query.exec()){
        qDebug() << "query failed! settings table creation failed";
        return false;
    } else {
        qDebug() << "query successful! settings table created";
        query.finish();
    }

    query.prepare("INSERT INTO settings(language, volFX, volMUSIC, fullscreen) VALUES (0,60,30,1)");

    if(!query.exec()){
        qDebug() << "query failed! settings set failed";
        return false;
    } else {
        qDebug() << "query successful! settings set";
        return true;
    }
}

void DBHelper::dropTable()
{
    QSqlQuery query;
    query.prepare("DROP TABLE profile");
    if(query.exec()) {
        qDebug() << "successfully dropped profile";
        query.finish();
        query.prepare("DROP TABLE settings");
        if(query.exec()) {
            qDebug() << "successfully dropeed settings";
        }
    }

    /*if(createTable()) {
        qDebug() << "table recreated";
    }*/
}

bool DBHelper::playerExists(QString name)
{
    QSqlQuery query;

    query.prepare("SELECT nickname FROM profile WHERE nickname = (:name)");
    query.bindValue(":name", name);

    if(query.exec()) {
        qDebug() << "p_exists executed";
        if(query.first()) {
            qDebug() << "player exists";
            return true;
        } else {
            return false;
        }
    } else {
        qDebug() << "p_exists failed";
        return false;
    }
}

bool DBHelper::changeName(QString newname) {
    QSqlQuery query;

    query.prepare("UPDATE profile SET nickname = (:newname) WHERE active = 1");
    query.bindValue(":newname", newname);

    if(query.exec()) {
        qDebug() << "name changed";
        return true;
    } else {
        return false;
    }
}

void DBHelper::getNames() {
    QSqlQuery query;
    query.exec("SELECT nickname FROM profile");
    while(query.next()) {//testing
        qDebug() << query.value(0).toString();
        //return query.value(0).toString();
    }
}

// wl - 1 = win, 0 = loss
bool DBHelper::updateStats(int cp, int wl, int d, int n, int spec, QString foes){
    QSqlQuery query;
    int w = 0;
    int l = 0;

    if(wl >= 1) {
        w = wl;
    } else {
        l = 1;
    }

    int* oldstats = getStats();

    //TODO some defines or constants
    int new_exp = cp + (w * WIN_POINTS) + (l * LOSS_POINTS) + (d * DEFUSE_POINTS) + (n * NOPE_POINTS) + (spec * SPECIAL_POINTS) + oldstats[6];

    cp = cp + oldstats[0];
    w = w + oldstats[1];
    l = l + oldstats[2];
    d = d + oldstats[3];
    n = n + oldstats[4];
    spec = spec + oldstats[5];

    //qDebug() << oldstats[4];

    query.prepare("UPDATE profile SET cardsPlayed = (:cp), wins = (:w), losses = (:l), defused = (:d), nopes = (:n), special = (:spec), exp = (:new_exp), lfoes = (:foes) WHERE active = 1");
    query.bindValue(":cp", cp);
    query.bindValue(":w", w);
    query.bindValue(":l", l);
    query.bindValue(":d", d);
    query.bindValue(":n", n);
    query.bindValue(":spec", spec);
    query.bindValue(":new_exp", new_exp);
    query.bindValue(":foes", foes);

    if(query.exec()) {
        qDebug() << "stats updated changed";
        return true;
    } else {
        qDebug() << "stats update failed";
        return false;
    }
}

// Array-Content:
// 0 - cardsPlayed
// 1 - wins
// 2 - losses
// 3 - defused
// 4 - nopes
// 5 - special
// 6 - exp
// 7 - level
// 8 - exp required for next level
int* DBHelper::getStats(){
    QSqlQuery query;

    query.prepare("SELECT * FROM profile WHERE active = 1");

    int* stats = new int[9];

    if(query.exec()) {
        if(query.first()) {
            for(int i = 1; i <= 7; i++) {
                stats[i - 1] = query.value(i).toInt();
            }
        }
    }

    int exp = stats[6];
    int level_req_exp = 1000;
    int level = 1;

    while (level_req_exp <= exp) {
        if(level_req_exp <= exp) {
            level++;
            level_req_exp += level_req_exp * 1.5; //oder vergleichbarer Wert
        }
    }

    stats[7] = level;
    stats[8] = level_req_exp;

    return stats;
}

QVariantList DBHelper::getActivePlayerStats()
{
    QSqlQuery query;

    query.prepare("SELECT * FROM profile WHERE active = 1");

    QVariantList stats;

    if(query.exec()) {
        if(query.first()) {
            for(int i = 0; i < query.record().count() - 1; i++) {
                stats.push_back(query.value(i).toString());
                qDebug() << query.value(i).toString();
            }
        }
    }

    int exp = stats.at(7).toInt();
    int level_req_exp = 1000;
    int level = 1;

    while (level_req_exp <= exp) {
        if(level_req_exp <= exp) {
            level++;
            level_req_exp += level_req_exp * 1.5; //oder vergleichbarer Wert
        }
    }

    stats.push_back(level);
    stats.push_back(level_req_exp);

    return stats;
}

QString DBHelper::getActiveProfile() {
    QSqlQuery query;

    query.prepare("SELECT nickname FROM profile WHERE active = 1");

    QString prof = "";

    if(query.exec()) {
        if(query.first()) {
            qDebug() << "DBHELPER::getActiveProfile(): " << query.value(0).toString();
            prof = query.value(0).toString();
        }
    }

    return prof;
}

std::vector<int> DBHelper::getSettings()
{
    QSqlQuery query;

    std::vector<int> settings;

    query.prepare("SELECT * FROM settings");

    if(query.exec()) {
        if(query.first()) {
            for(int i = 0; i < SETTINGSCOUNT; i++) {
                settings.push_back(query.value(i).toInt());
            }
        }
    }

    return settings;
}

bool DBHelper::setSettings(int language, int volFX, int volMUSIC, int fullscreen)
{
    QSqlQuery query;

    query.prepare("UPDATE settings SET language = (:lang), volFX = (:volF), volMUSIC = (:volM), fullscreen = (:fs)");
    query.bindValue(":lang", language);
    query.bindValue(":volF", volFX);
    query.bindValue(":volM", volMUSIC);
    query.bindValue(":fs", fullscreen);

    if(query.exec()) {
        qDebug() << "[DBHELPER] settings updated";
        return true;
    } else {
        return false;
    }
}
