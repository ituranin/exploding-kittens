#ifndef NETWORKHEADERS_H
#define NETWORKHEADERS_H

//headers
static const quint8 TESTHEADER = 0;
static const quint8 SENDDISCONNECT = 1; // if player disconnects himself
static const quint8 PLAYERDISCONNECTED = 2; // if player loses connection
static const quint8 STATECHANGED = 3;
static const quint8 PLAYCARD = 4;
static const quint8 PLAYERADD = 5;
static const quint8 ACTIVEPLAYER = 6;
static const quint8 SENDGAMESTART = 7;
static const quint8 KICK = 8;
static const quint8 LOST = 9;
static const quint8 WON = 10;
static const quint8 NEWPLAYERLIST = 11;
static const quint8 SENDCARDS = 12;
static const quint8 GAMEOVER = 13;
static const quint8 SENDCARD = 14;
static const quint8 SENDPLAYEDCARDS = 15;
static const quint8 CARDAMOUNT = 16;
static const quint8 SEETHEFUTURE = 17;
static const quint8 SHUFFLE = 18;
static const quint8 DRAWPILECOUNT = 19;
static const quint8 FULLLOBBY = 20;
static const quint8 SENDIP = 21;
static const quint8 PLAYERSTATS = 22;
static const quint8 GAMESTATS = 23;
static const quint8 GAMERUNNING = 24;
static const quint8 PROGRESS = 25;
static const quint8 KITTENPLACEMENT = 26;
static const quint8 TAKECARD = 27;

#endif // NETWORKHEADERS_H
