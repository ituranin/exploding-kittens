#include "logging.h"


Logging::Logging()
{    
    log.insert(logVals::amountDefuse, 0);
    log.insert(logVals::amountNope, 0);
    log.insert(logVals::amountCards, 0);
    log.insert(logVals::amountSpecialMoves, 0);
}

void Logging::playedCard(AbstractCard::CardTypes card)
{
    qDebug() << "[LOGGING] add log for: " << card;

    //count every played card
    auto iter = log.find(logVals::amountCards);
    ++iter.value();

    if (card == AbstractCard::Defuse){
        iter = log.find(logVals::amountDefuse);
        ++iter.value();
    }
    else if(card == AbstractCard::Nope){
        iter = log.find(logVals::amountNope);
        ++iter.value();

        if (firstNope){
            firstNope = false;
        }
        else
        {
            iter = log.find(logVals::amountSpecialMoves);
            ++iter.value();
        }
    }
}

void Logging::addSpecialMove()
{
    auto iter = log.find(logVals::amountSpecialMoves);
    ++iter.value();
}

void Logging::finishedNopingRound()
{
    qDebug() << "[LOGGING] noping finished";
    firstNope = true;
}

QMap<Logging::logVals, unsigned int> Logging::getReport() const
{
    return log;
}




