#include "server.h"

Server::Server(QObject *parent) : QObject(parent)
{
    inLobby = true;
    server = new QTcpServer(this);
    clients = new std::vector<QTcpSocket*>();
    connect(server, SIGNAL(newConnection()), this, SLOT(acceptClient()));
    if(!server->listen(QHostAddress::Any, port)) {
        qDebug() << "shiiiiiit, server not started";
    } else {
        qDebug() << "yey, server started on";
    }
}

Server::~Server()
{
    for(auto it = clients->begin(); it != clients->end();) {
        //(*it)->disconnectFromHost();
        disconnect((*it),0,0,0);
        it = clients->erase(it);
    }
    for(int i = 0; i < clients->size(); i++) {
        delete clients->at(i);
    }
    delete clients;
    server->close();
    disconnect(server, 0, 0, 0);
    delete server;
}

void Server::sendNetworkSignal(QByteArray& buffer)
{
    //server->waitForNewConnection();
    //clients->at(0)->write("Das ist ein Test");
    /*int i = 0;
    for(std::vector<QTcpSocket*>::iterator it = clients->begin(); it < clients->end(); it++,i++){
        clients->at(i)->write(buffer);
    }*/
    for(auto it = clients->begin(); it != clients->end();){
        (*it)->write(buffer);
        ++it;
    }
    //    qDebug() << "server sent message to" << clients->size();
}

void Server::sendNetworkSignalTo(QString ip, QByteArray &buffer)
{
    /*int i = 0;
    for(std::vector<QTcpSocket*>::iterator it = clients->begin(); it < clients->end(); it++,i++){
        if(ip == clients->at(i)->peerAddress().toString()) {
            clients->at(i)->write(buffer);
            qDebug() << "network package sent to " + ip;
        }
    }*/
    for(auto it = clients->begin(); it != clients->end();){
        if((*it)->peerAddress().toString() == ip){
            (*it)->write(buffer);
            ++it;
        } else {
            ++it;
        }
    }
}

void Server::readNetworkSignal()
{
//    qDebug() << "readNetworkSignal() called";
    QTcpSocket* client = (QTcpSocket*)sender();
//    qDebug() << client->peerAddress();
    QByteArray buffer = client->readAll();
    QDataStream input(buffer);

    while(!input.atEnd()) {

        quint8 header;
        //read the header-number
        input >> header;
//        qDebug() << "header read";
        //QString str;
        // and do stuff, based on it
        switch (header) {
        case TESTHEADER: {
            QString str;
            input >> str;
//            qDebug() << header << str;
            // emit signal(with params)
            break;
        }

        case SENDDISCONNECT: {
//            qDebug() << header << "disconnect received";
            // emit signal(with params)
            break;
        }

        case PLAYCARD: {
            qDebug() << "[SERVER] received PLAYCARD";
            std::vector<AbstractCard::CardTypes>* cards = new std::vector<AbstractCard::CardTypes>;
            quint32 size;
            input >> size;

            for(size_t i = 0; i < size; i++) {
                int card;
                input >> card;
//                qDebug() << card;
                cards->push_back(static_cast<AbstractCard::CardTypes>(card));
            }

            QString ip = client->peerAddress().toString();
//            qDebug() << ip;
            emit clientPlayedCardsReceived(ip, *cards);
            // emit signal(with params)
            break;
        }

        case PLAYERADD: {
            qDebug() << "[SERVER] received PLAYERADD";
            QString name;
            input >> name;
            QString ip = client->peerAddress().toString();
            emit addPlayer(name, ip);
            // emit signal(with params)
            break;
        }

        case KITTENPLACEMENT: {
            qDebug() << "[SERVER] received KITTENPLACEMENT";
            int position;
            input >> position;
            QString ip = client->peerAddress().toString();
            emit sig_placeKitten(ip, position);
        }



        default:
            break;
        }
    }
}

void Server::stateChanged(BaseState::playStates state)
{
    qDebug() << "[Server] send STATECHANGED to " << state;
    QByteArray buffer;
    QDataStream output(&buffer, QIODevice::WriteOnly);
    output << STATECHANGED << state;
    sendNetworkSignal(buffer);
}

void Server::sendPlayerList(std::vector<publicPlayer *> *players)
{
    QByteArray buffer;
    QDataStream output(&buffer, QIODevice::WriteOnly);
    output << NEWPLAYERLIST << (quint32)players->size();
    for(publicPlayer* player : *players) {
        output << QString(player->getPlayerName()) << QString(player->getIPAddress());
    }
    sendNetworkSignal(buffer);
    qDebug() << "[Server] send NEWPLAYERLIST ";
}

void Server::sendActivePlayer(QString ip)
{
    qDebug() << "[Server] sends ACTIVEPLAYER, IP: " << ip;
    QByteArray buffer;
    QDataStream output(&buffer, QIODevice::WriteOnly);
    output << ACTIVEPLAYER << ip;
    sendNetworkSignal(buffer);
}

void Server::sendGameIsStarting(){
    inLobby = false;
    qDebug() << "[Server] sends SENDGAMESTART: ";
    QByteArray buffer;
    QDataStream output(&buffer, QIODevice::WriteOnly);
    output << SENDGAMESTART;
    sendNetworkSignal(buffer);
}

void Server::disconnectPlayer()
{
    qDebug() << "[Server] send PLAYERDISCONNECTED ";
    QTcpSocket* client = (QTcpSocket*)sender();
    QByteArray buffer;
    QDataStream output(&buffer, QIODevice::WriteOnly);
    output << PLAYERDISCONNECTED << client->peerAddress().toString();
    sendNetworkSignal(buffer);
    deleteClientSocket(client->peerAddress().toString());
}

void Server::kickPlayer(QString ip)
{
    qDebug() << "[Server] send KICK " + ip;
    QByteArray buffer;
    QDataStream output(&buffer, QIODevice::WriteOnly);
    output << KICK << ip;
    sendNetworkSignalTo(ip, buffer);

    deleteClientSocket(ip);
}

void Server::announceWinner(QString ip)
{
    qDebug() << "Server: announces winner " + ip;
    QByteArray buffer;
    QDataStream output(&buffer, QIODevice::WriteOnly);
    output << WON << ip;
    sendNetworkSignal(buffer);
}

void Server::announceLooser(QString ip)
{
    qDebug() << "Server: announces looser " + ip;
    QByteArray buffer;
    QDataStream output(&buffer, QIODevice::WriteOnly);
    output << LOST << ip;
    sendNetworkSignal(buffer);
}

void Server::slt_sendCards(QString ip, std::vector<AbstractCard::CardTypes> cards)
{
    //DONE NOT TESTED
    qDebug() << "[Server] sends SENDCARDS, amount is: " << cards.size();
    QByteArray buffer;
    QDataStream out(&buffer, QIODevice::WriteOnly);
    out << SENDCARDS << (quint32)cards.size();
    for (AbstractCard::CardTypes card : cards) {
        out << card;
    }
    sendNetworkSignalTo(ip, buffer);
}

void Server::slt_sendCard(QString ip, AbstractCard::CardTypes cardType)
{
    qDebug() << "[Server] sends SENDCARD " << cardType << " to ip " << ip;
    QByteArray buffer;
    QDataStream out(&buffer, QIODevice::WriteOnly);
    out << SENDCARD << cardType;
    sendNetworkSignalTo(ip, buffer);
}

void Server::slt_sendPlayedCards(QString ip, std::vector<AbstractCard::CardTypes> cards)
{
    qDebug() << "[Server] sends SENDPLAYEDCARDS played by " << ip << " to all";
    QByteArray buffer;
    QDataStream out(&buffer, QIODevice::WriteOnly);
    out << SENDPLAYEDCARDS << ip << (quint32)cards.size();
    for (AbstractCard::CardTypes card : cards) {
        out << card;
    }
    sendNetworkSignal(buffer);
}

void Server::slt_sendGameEnded()
{
    qDebug() << "[Server] sends GAMEOVER";
    QByteArray buffer;
    QDataStream out(&buffer, QIODevice::WriteOnly);
    out << GAMEOVER;
    sendNetworkSignal(buffer);
    inLobby = true;
}

void Server::slt_amountOfCardsChanged(QString ip, uint amount)
{
    qDebug() << "[Server] sends CARDAMOUNT ip: " << ip << " amount: " << amount;
    QByteArray buffer;
    QDataStream out(&buffer, QIODevice::WriteOnly);
    out << CARDAMOUNT << ip << amount;
    sendNetworkSignal(buffer);
}

void Server::slt_drawPileCountChanged(uint amount)
{
    qDebug() << "[Server] sends DRAWPILECOUNT, amount: " << amount;
    QByteArray buffer;
    QDataStream out(&buffer, QIODevice::WriteOnly);
    out << DRAWPILECOUNT << amount;
    sendNetworkSignal(buffer);
}

void Server::slt_sendSeeTheFutureCards(QString ip, std::vector<AbstractCard::CardTypes> cards)
{
    qDebug() << "[Server] sends SEETHEFUTURE to " << ip;
    QByteArray buffer;
    QDataStream out(&buffer, QIODevice::WriteOnly);
    out << SEETHEFUTURE << (quint32)cards.size();
    for (AbstractCard::CardTypes card : cards) {
        out << card;
    }
    sendNetworkSignalTo(ip, buffer);
}

void Server::slt_sendDeckShuffled()
{
    qDebug() << "[Server] sends SHUFFLE";
    QByteArray buffer;
    QDataStream out(&buffer, QIODevice::WriteOnly);
    out << SHUFFLE;
    sendNetworkSignal(buffer);
}

void Server::slt_clientDisconnected()
{
    qDebug() << "[SERVER] client disconnected received from socket";

    QTcpSocket* client = (QTcpSocket*)sender();
    QString ip  = client->peerAddress().toString();

    qDebug() << "[SERVER] client " << client->peerAddress().toString()<< " disconnected";
    deleteClientSocket(client->peerAddress().toString());
    emit sig_playerDisconnectedFromGame(ip);

}

void Server::deleteClientSocket(QString ip)
{
    qDebug() << "[SERVER SOCKETCOUNT] " << clients->size();

    qDebug() << ip << " equals? ";

    for(auto it = clients->begin(); it != clients->end();){
        if((*it)->peerAddress().toString() == ip) {
            bool dis = disconnect((*it), SIGNAL(disconnected()), this, SLOT(slt_clientDisconnected()));
            qDebug() << "[DISCONNECTING SERVER CLIENT SIGS_SLOTS]" << disconnect((*it),0,0,0);
            qDebug() << "disconnect: " << dis;
            //qDebug() << (*it)->peerAddress().toString();
            //(*it)->disconnectFromHost();
            (*it)->close();
            it = clients->erase(it);
            break;
        }
        else
            ++it;
    }
    qDebug() << "[SERVER SOCKETCOUNT] " << clients->size();

    //check if list is empty -> delete self
//    if (clients->size() < 1)
//        delete this;
}

void Server::closeSocket()
{
    QTcpSocket* client = (QTcpSocket*)sender();
    if(client != NULL) {
        qDebug() << "[SERVER] closing socket";
        client->close();
        delete client;
    }
}

void Server::slt_sendPlayerStatistics(QString ip, QMap<Logging::logVals, unsigned int> playerStatistics, QString playerList)
{
    qDebug() << "[SERVER] send player Satistics to player " << ip;
    QByteArray buffer;
    QDataStream out(&buffer, QIODevice::WriteOnly);
    out << PLAYERSTATS << playerStatistics << playerList;
    sendNetworkSignalTo(ip, buffer);
}

void Server::slt_sendGameStatistics(QMap<Logging::logVals, QMap<QString, unsigned int> > gameBestOfStatistics)
{
    qDebug() << "[SERVER] send game end Statistics to all";
    QByteArray buffer;
    QDataStream out(&buffer, QIODevice::WriteOnly);
    out << GAMESTATS << gameBestOfStatistics;
    sendNetworkSignal(buffer);
}

void Server::slt_sendProgress(double percentage)
{
//    qDebug() << "[SERVER] send remaining state time";
    QByteArray buffer;
    QDataStream out(&buffer, QIODevice::WriteOnly);
    out << PROGRESS << percentage;
    sendNetworkSignal(buffer);
}

void Server::slt_removeCards(QString ip, std::vector<AbstractCard::CardTypes> cards)
{
    qDebug() << "[SERVER] removing card [" << cards.at(0) <<"] from player ip [" << ip <<"]";
    QByteArray buffer;
    QDataStream out(&buffer, QIODevice::WriteOnly);
    out << TAKECARD << cards.at(0);
    sendNetworkSignalTo(ip, buffer);
}

void Server::acceptClient()
{
    QTcpSocket* client = server->nextPendingConnection();

    if(inLobby && clients->size() < MAXPLAYERCOUNT) {
        connect(client, SIGNAL(readyRead()), this, SLOT(readNetworkSignal()));
        connect(client, SIGNAL(disconnected()), this, SLOT(slt_clientDisconnected()));

        clients->push_back(client);

        QByteArray buffer;
        QDataStream output(&buffer, QIODevice::WriteOnly);
        output << SENDIP << client->peerAddress().toString();
        client->write(buffer);
    } else {
        qDebug() << "[SERVER] MAX player count reached!";
        connect(client, SIGNAL(disconnected()), this, SLOT(closeSocket()));
        QByteArray buffer;
        QDataStream output(&buffer, QIODevice::WriteOnly);
        if(inLobby) {
            output << FULLLOBBY;
        } else {
            output << GAMERUNNING;
        }
        client->write(buffer);
        //client->close();
    }
}
