#ifndef EMPTYTEST_H
#define EMPTYTEST_H
#include <QtTest>


/**
 * @brief The emptyTest class is a blank test class
 */
class emptyTest : public QObject
{

    Q_OBJECT

//private slots:
private Q_SLOTS:


//    void initTestCase(){
//        qDebug("Starting Decktests");
//    }


    void myEmptyTest(){
        QVERIFY(1 != 2);
    }

//    void cleanupTestCase(){
//        qDebug("called after myFirstTest and mySecondTest");
//    }


};

//QTEST_MAIN(decktest)
//#include "deckTest.moc"


#endif // EMPTYTEST_H
