#ifndef AUDIO_H
#define AUDIO_H
#include <QObject>
#include <QIODevice>
#include <QAudioOutput>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QtQml>

/**
 * @brief The Audio class is used for playing sounds in the game.
 */
class Audio : public QObject
{

    Q_OBJECT

public:
    /**
     * @brief Audio is the classes constructor
     */
    Audio();

    /**
     * @brief ~Audio is the classes destructor
     */
    ~Audio();

    /**
     * @brief The SoundTypes enum represents a sound or loop in both playlists.
     * Those enums stand for the indexes in the loopplaylist and playlist
     */
    enum SoundTypes {
        MenuLoop,
        Intro = MenuLoop,
        Meow1,
        InGameLoop = Meow1,
        Meow2,
        KittenDrawLoop = Meow2,
        Play1,
        DefuseLoop = Play1,
        Play2,
        Play3,
        Select1,
        Select2,
        Select3,
        Select4,
        Shuffle,
        AnnouncerAttack,
        AnnouncerDefuse,
        AnnouncerNope,
        AnnouncerYourTurn,
        AnnouncerWon,
        AnnouncerSkip,
        AnnouncerLost
    };

    Q_ENUM(SoundTypes)

    /**
     * @brief declareQML is called make the sound enums available in qml
     */
    static void declareQML() {
        qmlRegisterType<Audio>("SoundTypes", 1, 0, "Sound");
    }

public slots:
    /**
     * @brief playSound is used to play a sound or an announcer recording
     * @param index is the index of the sound to play in the according playlist
     */
    void playSound(int index);

    /**
     * @brief playLoop is used to play a background music
     * @param index is the index of the loop file in the loopplaylist
     */
    void playLoop(int index);

    /**
     * @brief stopSound is used to stop the sound that is currently being played
     */
    void stopSound();

    /**
     * @brief stopLoop is used to stop the currently played loop sound
     */
    void stopLoop();

    /**
     * @brief setVolume is used to set the volumes of both playlists
     * @param volFX is the volume of the sounds from 0-100
     * @param volLOOP is the volume of the background music from 0-100
     */
    void setVolume(int volFX, int volLOOP);

signals:

    /**
     * @brief playS is emitted when a sound should be played
     */
    void playS();

    /**
     * @brief playL is emitted when background music should be played
     */
    void playL();

    /**
     * @brief volS is emitted when the volume of sounds(fx) and announcer should be changed
     * @param volFX is the volume of the sounds from 0-100
     */
    void volS(int volFX);

    /**
     * @brief volL is emitted when the volume of the background music should be changed
     * @param volMUSIC is the volume of the music from 0-100
     */
    void volL(int volMUSIC);

    /**
     * @brief stopS is emitted when the currently played sound should be stopped
     */
    void stopS();

private:
    /**
     * @brief player is the audio player for the sounds and announcer recordings
     */
    QMediaPlayer* player;

    /**
     * @brief loopplayer is the audio player for background music
     */
    QMediaPlayer* loopplayer;

    /**
     * @brief playlist is the playlist which holds all fx sounds and announcer recordings
     */
    QMediaPlaylist* playlist;

    /**
     * @brief loopplaylist is the playlist which holds all background music loops
     */
    QMediaPlaylist* loopplaylist;

};

#endif // AUDIO_H
