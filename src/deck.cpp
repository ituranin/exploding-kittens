#include "deck.h"



Deck::Deck(DeckType deckType, int playerAmount):m_deckType(deckType){
    fillDeckWithCards(playerAmount);
}

int Deck::getDeckSize() const{
    return this->m_cards.size();
}


void Deck::addCard(AbstractCard::CardTypes cardType){

    //qDebug() << "adding Card: " << static_cast<AbstractCard::CardTypes>(cardType);
    QMap<AbstractCard::CardTypes,int>::iterator iter = m_cards.find(cardType);
    if (iter != m_cards.end() && iter.key() == cardType){
        ++iter.value();
        //qDebug() << iter.key() << " has now: " << iter.value() << " cards";
    }
}

void Deck::addCards(std::vector<AbstractCard::CardTypes> cards){
    for (auto bla = cards.begin(); bla != cards.end(); ++bla){
        auto iter = m_cards.find(*bla);
        ++iter.value();
    }
}

int Deck::countCards(){
    int returnWert = 0;

    for (auto i = Deck::m_cards.begin(); i != Deck::m_cards.end(); ++i){
        returnWert += i.value();
    }
    //qDebug() << "count returns: " << returnWert;
    return returnWert;
}

void Deck::printRemainingCards(){
    QMap<AbstractCard::CardTypes,int>::iterator i;
    for (i = Deck::m_cards.begin(); i != Deck::m_cards.end(); ++i)
        qDebug() << i.key() << "= " << i.value();
}

void Deck::fillDeckWithCards(int playerAmount){

    //fresh deck to play with
    if (m_deckType == DrawPile){

        //create a Full Deck depending of the player amount

        //        Deck::m_cards.insert(AbstractCard::Defuse,(6 - m_playerAmountToCreateDeck));             //Defuse Card           -> 6 times minus the amount of players
        Deck::m_cards.insert(AbstractCard::Defuse,6);             //Defuse Card           -> 6 times
        Deck::m_cards.insert(AbstractCard::ExplosiveKitten,(playerAmount-1));    //ExplosiveKitten Card  -> 4 times (max at 5 players)
        //46 Cards from here are standart
        Deck::m_cards.insert(AbstractCard::Attack,4);             //Attack Card           -> 4 times
        Deck::m_cards.insert(AbstractCard::Favor,4);              //Favor Card            -> 4 times
        Deck::m_cards.insert(AbstractCard::Nope,5);               //Nope Card             -> 5 times
        Deck::m_cards.insert(AbstractCard::SeeTheFuture,5);       //SeeTheFuture Card     -> 5 times
        Deck::m_cards.insert(AbstractCard::Shuffle,4);            //Shuffle Card          -> 4 times
        Deck::m_cards.insert(AbstractCard::Skip,4);               //Skip Card             -> 4 times
        Deck::m_cards.insert(AbstractCard::BikiniCat,4);          //BikiniCat             -> 4 times
        Deck::m_cards.insert(AbstractCard::CatsSchroedinger,4);   //CatsSchroedinger      -> 4 times
        Deck::m_cards.insert(AbstractCard::MommaCat,4);           //MommaCat              -> 4 times
        Deck::m_cards.insert(AbstractCard::ShyBladder,4);         //ShyBladder            -> 4 times
        Deck::m_cards.insert(AbstractCard::ZombieCat,4);          //ZombieCat             -> 4 times
    }else{

        //create an empty Deck to put something into

        Deck::m_cards.insert(AbstractCard::Defuse,0);
        Deck::m_cards.insert(AbstractCard::ExplosiveKitten,0);
        Deck::m_cards.insert(AbstractCard::Attack,0);
        Deck::m_cards.insert(AbstractCard::Favor,0);
        Deck::m_cards.insert(AbstractCard::Nope,0);
        Deck::m_cards.insert(AbstractCard::SeeTheFuture,0);
        Deck::m_cards.insert(AbstractCard::Shuffle,0);
        Deck::m_cards.insert(AbstractCard::Skip,0);
        Deck::m_cards.insert(AbstractCard::BikiniCat,0);
        Deck::m_cards.insert(AbstractCard::CatsSchroedinger,0);
        Deck::m_cards.insert(AbstractCard::MommaCat,0);
        Deck::m_cards.insert(AbstractCard::ShyBladder,0);
        Deck::m_cards.insert(AbstractCard::ZombieCat,0);
    }
}



/*
 * DrawDeck
 * from here
 *
 * */

DrawDeck::DrawDeck(int playerAmount):Deck(DeckType::DrawPile, playerAmount), m_playerAmountToCreateDeck(playerAmount){
    m_deck = new QList<AbstractCard::CardTypes>;
}

AbstractCard::CardTypes DrawDeck::drawCard()
{
    qDebug() << "drawing a card from the draw pile";
    if (m_setupFinished){
//        qDebug() << "taking top card...";
        return m_deck->takeFirst();
    }
    else{
        return AbstractCard::ERRORCARD;
    }
}

AbstractCard::CardTypes DrawDeck::getSpecificCard(AbstractCard::CardTypes cardType){

//    qDebug() << "getSpecificCard: " << static_cast<AbstractCard::CardTypes>(cardType);
    QMap<AbstractCard::CardTypes,int>::iterator iter = m_cards.find(cardType);

    if (iter.value() > 0){
        --iter.value();

//        qDebug() << "returning: " << static_cast<AbstractCard::CardTypes>(cardType) << " amount left: " << iter.value();
        return cardType;
    }
//    qDebug() << "returning: " << static_cast<AbstractCard::CardTypes>(AbstractCard::ERRORCARD);
    return AbstractCard::ERRORCARD;
}

void DrawDeck::addKittenAtPosition(AbstractCard::CardTypes kitten, int position){
    if (kitten == AbstractCard::ExplosiveKitten && m_setupFinished){
        if (position < 0)
            position = 0;

        if (position > m_deck->size())
            position = m_deck->size();
        qDebug() << "[DRAWPILE] Put kitten at position: " << position << " of: " << m_deck->size();
        m_deck->insert(position, AbstractCard::ExplosiveKitten);
        qDebug() << "[DRAWPILE] new size: " << m_deck->size() << " and kitten is @" << m_deck->at(position);
        printRemainingCards();
    }
    else{
        qDebug() << "[DRAWPILE] received " << kitten << " @ " << position << " of: " << getDeckSize() << " with " << m_setupFinished;
    }
}

void DrawDeck::shuffleDeck()
{
    QList<AbstractCard::CardTypes>* newShuffledDeck = new QList<AbstractCard::CardTypes>;
    QList<AbstractCard::CardTypes>* toDeleteList = new QList<AbstractCard::CardTypes>;
    int randomPick;

    while (m_deck->size() > 0){
        randomPick = rand() % m_deck->size();     //randomPick is card to be picked between 0 and size
        newShuffledDeck->push_back(m_deck->takeAt(randomPick));
    }

    toDeleteList = m_deck;
    m_deck = newShuffledDeck;

    newShuffledDeck = NULL;

    delete toDeleteList;
    delete newShuffledDeck;
}



std::vector<AbstractCard::CardTypes> DrawDeck::derivePlayerDeck()
{
    std::vector<AbstractCard::CardTypes> returnCards;

    if (m_decksDerived < m_playerAmountToCreateDeck){
        //add 1 defuse card
        returnCards.push_back(getSpecificCard(AbstractCard::Defuse));

        //add 4 random cards not being a kitten or defuse
        for (int i = 0; i < 4; ++i) {
            returnCards.push_back(getRandomNotDefuseNorKittenCard());
        }
    }
    ++m_decksDerived;

    return returnCards;
}

void DrawDeck::adjustDeckBecauseofPlayerLeave(unsigned int amountOfPlayer)
{
    if (amountOfPlayer < m_playerAmountToCreateDeck){   //useless test cannot adjust m_playerAmountToCreateDeck would be wrong

        //a player left so one kitten needs to be removed
        getSpecificCard(AbstractCard::ExplosiveKitten);

        //and the deck needs to be shuffled
        shuffleDeck();
    }
}

std::vector<AbstractCard::CardTypes> DrawDeck::activateSeeTheFuture()
{
    std::vector<AbstractCard::CardTypes> stfCards = {m_deck->at(0), m_deck->at(1), m_deck->at(2)};
    return stfCards;
}

AbstractCard::CardTypes DrawDeck::getRandomNotDefuseNorKittenCard(){

        AbstractCard::CardTypes cardType = AbstractCard::ERRORCARD;

        while (cardType == AbstractCard::Defuse || cardType == AbstractCard::ExplosiveKitten || cardType == AbstractCard::ERRORCARD){
            qDebug() << "getRandomNotKittenOrDefuse runs again because of: " << static_cast<AbstractCard::CardTypes>(cardType);

            //get random card
            cardType = getRandomCard();
            qDebug() << "getRandomNotKittenOrDefuse new cardType is: " << static_cast<AbstractCard::CardTypes>(cardType);

            //if card is defuse / kitten, put it back
            if (cardType == AbstractCard::Defuse || cardType == AbstractCard::ExplosiveKitten){
                qDebug() << "getRandomNotKittenOrDefuse cardType put back";
                addCard(cardType);
            }

        }


    qDebug() << "getRandomNotKittenOrDefuse returns: " << static_cast<AbstractCard::CardTypes>(cardType);

    return cardType;
}

AbstractCard::CardTypes DrawDeck::getRandomCard(){

    int size = m_cards.size();

    int randomPick = rand() % size;     //randomPick is card to be picked between 0 and size
//    qDebug() << "randomPick is: " << randomPick << " - out of " << size;
//    qDebug() << "Random Card is of Type of: " << static_cast<AbstractCard::CardTypes>(randomPick);

    AbstractCard::CardTypes returnCard = getSpecificCard(static_cast<AbstractCard::CardTypes>(randomPick));

//    qDebug() << "getRandomCard returns: " << static_cast<AbstractCard::CardTypes>(returnCard);

    return returnCard;
}






void DrawDeck::finalizeDeck()
{
    if (m_decksDerived == m_playerAmountToCreateDeck){

        AbstractCard::CardTypes randomCard;
        //1. create the array of cards for the final pile
        while(countCards() > 0)
        {
            //pick random card and push back to the randomized bla
            //really bad solution but it works :)
            randomCard = getRandomCard();
            if (randomCard != AbstractCard::ERRORCARD)
                m_deck->push_back(randomCard);
        }

        //2. shuffle
        shuffleDeck();

        //3. set deck finalized
        m_setupFinished = true;
    }
    else
        qDebug() << "not all player decks created - derived: " << m_decksDerived << " ,m_playerAmountToCreateDeck: " << m_playerAmountToCreateDeck;
}

int DrawDeck::countCards()
{
    if (!m_setupFinished){
        int returnInt = Deck::countCards();
        //qDebug() << returnInt;
        return returnInt;
    }
    else{
        return m_deck->size();
    }
}

void DrawDeck::printRemainingCards(){
    if (!m_setupFinished){
        Deck::printRemainingCards();
    }
    else
    {
        for(auto bla = m_deck->begin(); bla != m_deck->end(); ++bla){
            qDebug() << (*bla);
        }
    }
}


//test Methods

AbstractCard::CardTypes DrawDeck::readFirstCard() const
{
    return m_deck->at(0);
}

AbstractCard::CardTypes DrawDeck::getCardAtPosition(int i)
{
    if (m_deck->size() > i && i >= 0){
        return m_deck->takeAt(i);
    }
    return AbstractCard::ERRORCARD;
}



/*
 * PlayerDeck
 * from here
 *
 * */

//creates a PlayerStartDeck containing 1 defuse Cat and 4 random Cards from the PlayDeck as reference
PlayerDeck::PlayerDeck(){}

PlayerDeck::PlayerDeck(DrawDeck *refToPlayDeck):Deck(DeckType::HandCards), m_referenceDeck(refToPlayDeck)
{


    auto derivedDeck = refToPlayDeck->derivePlayerDeck();
    qDebug() << "creating hand deck, got derivedDeck containing: " << derivedDeck.size() << " cards";

    for (auto bla = derivedDeck.begin(); bla != derivedDeck.end(); ++bla){

        qDebug() << "assigning card: " << static_cast<AbstractCard::CardTypes>(*bla);
        addCard(*bla);
    }

    qDebug() << "Deck contains" << countCards() << "Cards";
}


AbstractCard::CardTypes PlayerDeck::getRandomCard(){

    int randomPick;
    int size = m_cards.size();
    AbstractCard::CardTypes cardType;


    while (true){
        randomPick = rand() % size;     //randomPick is card to be picked between 0 and size
        qDebug() << "randomPick is: " << randomPick << " - out of " << size;
        qDebug() << "Random Card is of Type of: " << static_cast<AbstractCard::CardTypes>(randomPick);

        QMap<AbstractCard::CardTypes,int>::iterator iter = m_cards.find(static_cast<AbstractCard::CardTypes>(randomPick));
        qDebug() << iter.key() << "has: " << iter.value() << " left";
        if (iter.value() > 0){
            --iter.value();
            qDebug() << "return is: " << static_cast<AbstractCard::CardTypes>(randomPick);
            cardType = static_cast<AbstractCard::CardTypes>(randomPick);

            return cardType;
        }
    }
}

AbstractCard::CardTypes PlayerDeck::getSpecificCard(AbstractCard::CardTypes cardType){

        qDebug() << "[PlayerDeck] getSpecificCard: " << static_cast<AbstractCard::CardTypes>(cardType);
        QMap<AbstractCard::CardTypes,int>::iterator iter = m_cards.find(cardType);

        if (iter.value() > 0){
            --iter.value();

            qDebug() << "[PlayerDeck] returning: " << static_cast<AbstractCard::CardTypes>(cardType) << " amount left: " << iter.value();
            return cardType;
        }
        qDebug() << "[PlayerDeck] returning: " << static_cast<AbstractCard::CardTypes>(AbstractCard::ERRORCARD);
        return AbstractCard::ERRORCARD;
}


void PlayerDeck::readCards(QVector<AbstractCard::CardTypes> &refDeckForGui)
{
    for (auto i = Deck::m_cards.begin(); i != Deck::m_cards.end(); ++i){
        qDebug() << i.key() << "= " << i.value();
        for (int k = 0;k < i.value(); ++k){
            refDeckForGui.push_back(i.key());
        }
    }
}



void PlayerDeck::readDeckToSendToNetwork(std::vector<AbstractCard::CardTypes> *buffer)
{
    if(!sentToNetwork){
       for (auto i = Deck::m_cards.begin(); i != Deck::m_cards.end(); ++i){
           qDebug() << i.key() << "= " << i.value();
           for (int k = 0;k < i.value(); ++k){
               buffer->push_back(i.key());
           }
       }
       sentToNetwork = true;
    }
    else
        qDebug() << "playerdeck allready sent to network";
}

int PlayerDeck::getAmountOfCards(AbstractCard::CardTypes card)
{
//    qDebug() << "[Deck] returning amount of " << card << "in player Deck";
    if (m_cards.size() > 0){
        auto iter = m_cards.find(card);
//        qDebug() << "[Deck] amount is " << iter.value();
        return iter.value();
    }
    else
        return 0;


}
