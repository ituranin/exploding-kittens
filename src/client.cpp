#include "client.h"

Client::Client() : QObject()
{
    connect(&socket, SIGNAL(readyRead()), this, SLOT(readNetworkSignal()));

    connect(&socket, SIGNAL(connected()), this, SLOT(slt_connected()));
    connect(&socket, SIGNAL(disconnected()), this, SLOT(slt_disconnected()));
//    addresses = QNetworkInterface::allAddresses();
}

Client::~Client()
{
    socket.close();
}

void Client::establish(QString adress)
{
    QHostAddress addr(adress);
    socket.connectToHost(addr, port);
}

void Client::sendNetworkSignal(QByteArray& buffer)
{

    if(socket.state() == QAbstractSocket::ConnectedState){
        qDebug() << "finally connected";
        socket.write(buffer);
    } else {
        qDebug() << "no connection!";
    }
    qDebug() << "send message";
}

//bool Client::checkIP(QString ip)
//{
//    QString choppedIP = ip;
//    choppedIP.remove(0, 7);

//    qDebug() << "ip to look for: " << choppedIP;

//    bool me = false;

//    for(int i = 0; i < addresses.size(); i++) {
//        qDebug() << addresses[i].toString();
//        if(addresses[i].toString() == choppedIP) {
//            me = true;
//            qDebug() << "ip is mine";
//            break;
//        }
//    }
//    return me;
//}

void Client::addSelf(QString name)
{
    qDebug() << "add self";
    QByteArray buffer;
    QDataStream out(&buffer, QIODevice::WriteOnly);
    out << PLAYERADD << name;
    sendNetworkSignal(buffer);
}

void Client::readNetworkSignal()
{
//    qDebug() << "client sent readyRead";
    QByteArray buffer = socket.readAll();
    QDataStream input(buffer);

    while(!input.atEnd()) {
        quint8 header;
        //read the header-number
        input >> header;
        // and do stuff, based on it
        switch (header) {
        case STATECHANGED: {
            int state;
            input >> state;
            qDebug() << "[NET CLIENT] STATECHANGED received " << static_cast<BaseState::playStates>(state);
            emit sig_newStateReceived(static_cast<BaseState::playStates>(state));
            break;
        }

        case ACTIVEPLAYER: {
            QString ip;
            input >> ip;
            qDebug() << "[NET CLIENT] ACTIVEPLAYER received " << ip;
            emit activePlayerReceived(ip);
            // emit signal(with params)
            break;
        }

        case SENDCARDS: {
            qDebug() << "[NET CLIENT] SENDCARDS received";

            std::vector<AbstractCard::CardTypes> cards;
            quint32 size;
            input >> size;
            qDebug() << "amount is: " << size;

            for(size_t i = 0; i < size; i++) {
                int card;
                input >> card;
                qDebug() << card;
                cards.push_back(static_cast<AbstractCard::CardTypes>(card));
            }
            emit sig_receivedCards(cards);
            break;
        }

        case SENDGAMESTART: {
            qDebug() << "[NET CLIENT] SENDGAMESTART received";
            emit gameIsStarting();
            break;
        }

        case WON: {
            qDebug() << "[NET CLIENT] WON received";
            QString winner;
            input >> winner;
            emit playerWon(winner);
            break;
        }

        case LOST: {
            qDebug() << "[NET CLIENT] LOST received";
            QString looser;
            input >> looser;
            emit playerLost(looser);
            break;
        }

        case PLAYERDISCONNECTED: {
            qDebug() << "[NET CLIENT] PLAYERDISCONNECTED received";
            QString player;
            input >> player;
            emit playerDisconnected(player);
            break;
        }

        case KICK: {
            qDebug() << "[NET CLIENT] KICK received";
            QString player;
            input >> player;
            emit sig_errorMessage(tr("You were kicked!"));
            break;
        }

        case NEWPLAYERLIST: {
            qDebug() << "[NET CLIENT] NEWPLAYERLIST received";
            std::vector<publicPlayer *> *players = new std::vector<publicPlayer *>;
            quint32 size;
            input >> size;
            for(int i = 0; i < size; i++) {
                QString name;
                QString ip;
                input >> name;
                input >> ip;
                publicPlayer* player = new publicPlayer(name, ip);
                players->push_back(player);
            }
            emit sig_receivedNewPlayerlist(players);
            break;
        }

        case GAMEOVER: {
            qDebug() << "[NET CLIENT] GAMEOVER received";
            emit sig_gameEnded();
            break;
        }

        case SENDCARD: {
            qDebug() << "[NET CLIENT] SENDCARD received";
            int card;
            input >> card;
            emit sig_receivedCard(static_cast<AbstractCard::CardTypes>(card));
            break;
        }

        case SENDPLAYEDCARDS: {
            qDebug() << "[NET CLIENT] SENDPLAYEDCARDS received";
            QString ip;
            input >> ip;

            std::vector<AbstractCard::CardTypes>* cards = new std::vector<AbstractCard::CardTypes>;
            quint32 size;
            input >> size;

            for(size_t i = 0; i < size; i++) {
                int card;
                input >> card;
                qDebug() << card;
                cards->push_back(static_cast<AbstractCard::CardTypes>(card));
            }
            emit playedCardsReceived(ip, *cards);
            break;
        }

        case CARDAMOUNT: {
            qDebug() << "[NET CLIENT] CARDAMOUNT received";
            QString ip;
            input >> ip;

            uint amount;
            input >> amount;
            qDebug() << "[NET CLIENT] card amount is " << amount << " for " << ip;
            emit sig_receivedCardAmount(ip, amount);
            break;
        }

        case SEETHEFUTURE: {
            qDebug() << "[NET CLIENT] SEETHEFUTURE received";
            std::vector<AbstractCard::CardTypes> cards;
            quint32 size;
            input >> size;

            for(size_t i = 0; i < size; i++) {
                int card;
                input >> card;
                qDebug() << card;
                cards.push_back(static_cast<AbstractCard::CardTypes>(card));
            }
            emit sig_receivedSeeTheFutureCards(cards);
            break;
        }

        case SHUFFLE: {
            qDebug() << "[NET CLIENT] SHUFFLE received";
            emit sig_shuffleReceived();
            break;
        }

        case DRAWPILECOUNT: {
            qDebug() << "[NET CLIENT] DRAWPILECOUNT received";
            uint amount;
            input >> amount;
            qDebug() << "[NET CLIENT] draw pile amount is " << amount;
            emit sig_receivedDrawPileCount(amount);
            break;
        }

        case FULLLOBBY: {
            qDebug() << "[NET CLIENT] FULLLOBBY received";
            this->socket.disconnectFromHost();
            emit sig_errorMessage(tr("Lobby is full!"));
            break;
        }

        case GAMERUNNING: {
            qDebug() << "[NET CLIENT] GAMERUNNING received";
            this->socket.disconnectFromHost();
            emit sig_errorMessage(tr("You can't join a running game!"));
            break;
        }

        case SENDIP: {
            qDebug() << "[NET CLIENT] SENDIP received";
            QString ip;
            input >> ip;
            emit ipReceived(ip);
            break;
        }

        case PLAYERSTATS: {
            qDebug() << "[NET CLIENT] PLAYERSTATS received";
            QMap<int, unsigned int> playerStatistics;
            input >> playerStatistics;
            QString playerList;
            input >> playerList;
            emit sig_receivedPlayerStats(playerStatistics, playerList);
            break;
        }

        case GAMESTATS: {
            qDebug() << "[NET CLIENT] GAMESTATS received";
            QMap<int, QMap<QString, unsigned int> > gameBestOfStatistics;
            input >> gameBestOfStatistics;
            emit sig_receivedGameStats(gameBestOfStatistics);
            break;
        }

        case PROGRESS: {
//            qDebug() << "[NET CLIENT] PROGRESS received";
            double percentage;
            input >> percentage;
            emit sig_progressChangedReceived(percentage);
            break;
        }

        case TAKECARD: {
            qDebug() << "[NET CLIENT] TAKECARD received";
            int card;
            input >> card;
            emit sig_receivedRemoveCard((AbstractCard::CardTypes)card);
            break;
        }

        default:
            break;
        }
    }
}

void Client::playCards(std::vector<AbstractCard::CardTypes> cards)
{
    qDebug() << "Client: received playcards amount is: " << cards.size();
    QByteArray buffer;
    QDataStream out(&buffer, QIODevice::WriteOnly);
    out << PLAYCARD << (quint32)cards.size();
    for (AbstractCard::CardTypes card : cards) {
        out << card;
    }
    sendNetworkSignal(buffer);
}

void Client::sendDisconnect()
{
//    qDebug() << "send disconnect";
    QByteArray buffer;
    QDataStream out(&buffer, QIODevice::WriteOnly);
    out << SENDDISCONNECT;
    sendNetworkSignal(buffer);
}

void Client::slt_connected()
{
    emit sig_connected();
}

void Client::slt_disconnected()
{
    emit sig_disconnected();
}

void Client::slt_sendServerKittenPositionInDrawPile(int position)
{
    qDebug() << "[CLIENT] send kitten position";
    QByteArray buffer;
    QDataStream out(&buffer, QIODevice::WriteOnly);
    out << KITTENPLACEMENT << position;
    sendNetworkSignal(buffer);
}
