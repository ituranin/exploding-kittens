#Exploding Kittens
![promo_Anim.gif](https://bitbucket.org/repo/BArjaa/images/2510768159-promo_Anim.gif)

### Index ###
* [Languages](#markdown-header-languages)
* [Description](#markdown-header-description)
* [Content](#markdown-header-content)
* [Features](#markdown-header-features)

### Languages ###

* QT
* C++
* QML

### Description###

This was a Projekt due the course of "Development of Multimedia (Applications)" as part of the studies at the UAS HTW Berlin.
It was created as a Teamwork by [Georg Weidel](https://bitbucket.org/gee2k/) and [Igor Turanin](https://bitbucket.org/ituranin/)

### Content###

The Game is built after the famous card game "Exploding Kittens" [http://www.explodingkittens.com/](http://www.explodingkittens.com/)

We played the cardgame much by ourself so it was obvious to us that we create a version on our own.

### Features

* The game so far features all the single card gamerules (multicard is possible but with a derivate ruling)
* Network play (despite of some bugs :)
* Expirience system
* Situation dependent Audio
* Pixel Graphics and Animations

### technical features
(some)

* Client Server Architecture
* For Cheat avoiding, gamelogic on Server Side w. checks
* multi platform compatible*

*so far its been tested on PC,Mac,Linux and Android - PC is smoothest, Unix based Systems need another Audio branch.