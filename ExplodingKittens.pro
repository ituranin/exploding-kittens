TEMPLATE = app

QT += qml quick
QT += network
QT += testlib
QT += multimedia
QT += sql
CONFIG += c++11
CONFIG += testcase
CONFIG += resources_big

CONFIG += ordered

SOURCES += src/main.cpp \
    src/gameadapter.cpp \
    src/gameplay.cpp \
    src/player.cpp \
    src/deck.cpp \
    src/dbhelper.cpp \
    src/cards.cpp \
    src/deckTest.cpp \
    src/emptytest.cpp \
    src/audio.cpp \
    src/stateMachine.cpp \
    src/client.cpp \
    src/server.cpp \
    src/logging.cpp \
    src/dbhelpertest.cpp \
    src/gamerulestest.cpp

lupdate_only{
SOURCES += main.qml \
        JoinGameForm.qml \
        HostGameForm.qml \
        OptionsMenuForm.qml \
        StatisticsMenuForm.qml \
        Card.qml \
        ParticleSystem.qml \
        GameForm.qml \
        Player.qml \
        CardsToBePlayed.qml \
        LobbyPlayer.qml \
        MainMenu.qml \
        ErrorMessage.qml \
        PlayerPlayedCards.qml \
        CreateProfile.qml \
        ExplodingKitten.qml \
        ChooseKittenPosition.qml \
        Shuffle.qml \
        EndGameStats.qml
}

RESOURCES += qml.qrc \
    locale.qrc
RESOURCES += resources.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    src/gameadapter.h \
    src/gameplay.h \
    src/gameplaysettings.h \
    src/player.h \
    src/deck.h \
    src/dbhelper.h \
    src/cards.h \
    src/deckTest.h \
    src/emptytest.h \
    src/audio.h \
    src/stateMachine.h \
    src/client.h \
    src/server.h \
    src/networkheaders.h \
    src/logging.h \
    src/dbhelpertest.h \
    src/gamerulestest.h

QMAKE_MAC_SDK = macosx10.12

TRANSLATIONS = locale/de.ts

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

QMAKE_EXTRA_TARGETS += valgrind-check

valgrind.check.CONFIG = recursive


doc.commands = doxygen documentation/Doxyfile
QMAKE_EXTRA_TARGETS += doc

CONFIG -= debug_and_release
