import QtQuick 2.0

Item {
    id: item1
    property var players: [];

    /*
        \qmlmethod addJoinLobbyPlayer(name, ip)

        This method creates a lobby player item with name and ip from parameters
    */
    function addJoinLobbyPlayer(name, ip) {
        var component = Qt.createComponent("qrc:///LobbyPlayer.qml");
        var player = component.createObject(playercolumn, {"ip": ip, "playername.text": name, "btn.visible": false, "height": (playercolumn.height * 0.2)});

        if(player == null) {console.log("Player not created")}
        if( component.status == Component.Error )
            console.debug("Error:"+ component.errorString() );
        players.push(player);
    }

    /*
        \qmlmethod deleteJoinLobbyPlayer(ip)

        This method deletes the lobby player item with the same ip as in the parameter
    */
    function deleteJoinLobbyPlayer(ip) {
        for(var i = 0; i < players.length; i++) {
            if(ip == players[i].ip) {
                players[i].destroyThis();
                delete players[i];
                players.sort();
                players.pop();
                break;
            }
        }
    }

    /*
        \qmlmethod wipePlayers()

        This method deletes all lobby player items
    */
    function wipePlayers() {
        for(var i = 0; i < players.length; i++) {
            players[i].destroyThis();
        }
        players = [];
    }

    Rectangle {
        id: ipWrapper
        height: parent.height * 0.3
        color: "#00000000"
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0

        Rectangle {
            id: labelJoin
            color: "#00000000"
            width: parent.width * 0.5
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0

            Text {
                id: txt_ipAdress
                height: parent.height * 0.5
                text: qsTr("IP:") + gameAdapter.translation
                anchors.top: parent.top
                anchors.topMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.right: parent.right
                anchors.rightMargin: 0
                font.pixelSize: 22
                font.bold: true
            }

            Rectangle {
                id: rectJoinButton
                height: parent.height * 0.5
                color: clickAreaJoinButton.containsMouse ? "#99000000" : "#99bada55"
                anchors.top: txt_ipAdress.bottom
                anchors.topMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0

                Text {
                    id: txtJoinButton
                    text: qsTr("Join Game") + gameAdapter.translation
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.fill: parent
                    font.pixelSize: 22
                    font.bold: true
                    color: "#ededed"
                }

                MouseArea {
                    id: clickAreaJoinButton
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        gameAdapter.joinGame();
                    }
                }
            }
        }

        Rectangle {
            id: inputAndLeave
            color: "#00000000"
            width: parent.width * 0.5
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0

            Rectangle {
                id: rectEnterIp
                height: parent.height * 0.5
                color: "#99000000"
                anchors.top: parent.top
                anchors.topMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0

                TextInput {
                    id: inputIpAdress
                    color: "#ededed"
                    text: gameAdapter.qmlReadIpAdress
                    cursorVisible: true
                    font.bold: true
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right
                    anchors.left: parent.left
                    visible: true
                    horizontalAlignment: Text.AlignHCenter
                    font.pixelSize: 22
                    onTextChanged: gameAdapter.qmlReadIpAdress = text;
                }
            }

            Rectangle {
                id: rectLeaveButton
                height: parent.height * 0.5
                color: clickAreaLeaveButton.containsMouse ? "#99000000" : "#99d14141"
                anchors.top: rectEnterIp.bottom
                anchors.topMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                Text {
                    id: txtLeaveButton
                    color: "#ededed"
                    text: qsTr("Leave") + gameAdapter.translation
                    verticalAlignment: Text.AlignVCenter
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    font.pixelSize: 22
                    font.bold: true
                }

                MouseArea {
                    id: clickAreaLeaveButton
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        gameAdapter.clientLeaveGame();
                        wipePlayers();
                    }
                }
            }
        }
    }

    Column {
        id: playercolumn
        anchors.top: ipWrapper.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.topMargin: 0
    }


}
