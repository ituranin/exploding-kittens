import QtQuick 2.0

Item {
    id: item1
    width: parent.width
    height: 400
    property var players: [];

    /*
        \qmlmethod addHostLobbyPlayer(name, ip)

        This method creates a lobby player item with ip and name from parameters
    */
    function addHostLobbyPlayer(name, ip) {
        var component = Qt.createComponent("qrc:///LobbyPlayer.qml");
        var player = component.createObject(playercolumn, {"ip": ip, "playername.text": name});

        if(player == null) {console.log("Player not created")}
        if( component.status == Component.Error )
            console.debug("Error:"+ component.errorString() );
        players.push(player);
    }

    /*
        \qmlmethod deleteHostLobbyPlayer(ip)

        This method deletes the lobby player item with the same ip as in the parameter
    */
    function deleteHostLobbyPlayer(ip) {
        for(var i = 0; i < players.length; i++) {
            if(ip == players[i].ip) {
                players[i].destroyThis();
                delete players[i];
                players.sort();
                players.pop();
                break;
            }
        }
    }

    Rectangle {
        id: hostGameWrapper
        height: parent.height * 0.3
        color: "#00000000"
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0

        Rectangle {
            id: rectHostGame
            height: parent.height * 0.5
            color: clickAreaHostButton.containsMouse ? "#99ffffff" : "#99000000"
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0

            Text {
                id: txtHostButton
                color: clickAreaHostButton.containsMouse ? "#444444" : "#ededed"
                text: qsTr("Start Local Host") + gameAdapter.translation
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.fill: parent
                font.pixelSize: 22
                font.bold: true
            }

            MouseArea {
                id: clickAreaHostButton
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    gameAdapter.hostGame();
                }
            }
        }

        Rectangle {
            id: rectStartGame
            height: parent.height * 0.5
            color: clickAreaStartButton.containsMouse ? "#99000000" : "#99bada55"
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.top: rectHostGame.bottom
            anchors.topMargin: 0
            Text {
                id: txtStartButton
                color: "#ededed"
                text: qsTr("Start Game") + gameAdapter.translation
                anchors.fill: parent
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 22
                font.bold: true
                horizontalAlignment: Text.AlignHCenter
            }

            MouseArea {
                id: clickAreaStartButton
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    gameAdapter.startHostedGame();
                }
            }
        }
    }

    Column {
        id: playercolumn
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: hostGameWrapper.bottom
        anchors.topMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
    }

    Text {
        id: txtWaitingForPLayers
        color: "#ededed"
        text: qsTr("waiting for players") + gameAdapter.translation
        anchors.top: parent.bottom
        anchors.topMargin: 0
        font.bold: true
        anchors.horizontalCenter: parent.horizontalCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 22
    }


}
